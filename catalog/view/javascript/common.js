$(document).ready(function() {
	/* Search */
	$('.button-search').bind('click', function() {
		url = $('base').attr('href') + 'index.php?route=product/search';
				 
		var search = $('input[name=\'search\']').attr('value');
		
		if (search) {
			url += '&search=' + encodeURIComponent(search);
		}
		
		location = url;
	});
	
	$('#header input[name=\'search\']').bind('keydown', function(e) {
		if (e.keyCode == 13) {
			url = $('base').attr('href') + 'index.php?route=product/search';
			 
			var search = $('input[name=\'search\']').attr('value');
			
			if (search) {
				url += '&search=' + encodeURIComponent(search);
			}
			
			location = url;
		}
	});
	
	var header = $('header');
	var body = $('body');

	$(window).scroll(function(){
		if($(window).scrollTop()>250){
			header.addClass('active').addClass('show');
			body.addClass('fix-menu');
		}else{
			header.removeClass('show').removeClass('active');
			body.removeClass('fix-menu');
		}
	});

	/* Страница нанесение */

	$('.js-toggle-active').click(function(){
		//$('body .js-toggle-active.active').removeClass('active');
		$(this).toggleClass('active');
	});

	$('.s-calculator-color .select-box-option').each(function(){
			var tColor=$(this).attr('data-color');
			$(this).append('<div class="boxcolor" style="background:'+tColor+'"></div>');
	});




	/* Подсчёт калькулятора */


    	        var quantity=$('.s-calculator-quantity .select-box-current').attr('value');
		        var size=$('input[name="bag-size"]:checked').val();
		        var color=$('.s-calculator-color .select-box-current').attr('value');
		        var thick=parseInt($('.s-calculator-thick .select-box-current').attr('value'));
		        var colors_quantity=parseInt($('.s-calculator-colors .select-box-current').attr('value'));
		        var isGold=$('input[name="isGold"]').val();
		        var pricePerItem=0;


        function getCurrentValues(){
    	        quantity=$('.s-calculator-quantity .select-box-current').attr('value');
		        size=$('input[name="bag-size"]:checked').val();
		        color=$('.s-calculator-color .select-box-current').attr('value');
		        thick=parseInt($('.s-calculator-thick .select-box-current').attr('value'));
		        colors_quantity=parseInt($('.s-calculator-colors .select-box-current').attr('value'));
		        isGold=$('input[name="isGold"]').val();
		        pricePerItem=0;

        }

        function showCurrentValues(){
            console.log('quantity: '+quantity);
            console.log('size: '+size);
            console.log('color: '+color);
            console.log('thick: '+thick);
            console.log('colors_quantity: '+colors_quantity);
            console.log('isGold: '+isGold);
            console.log('-----');
        }

        function getPriceByCurrentValues(){           
            $table=$('#dataBags');
            $row = $table.find('tr[data-quantity='+quantity+']');
            

            // Пакеты 20х30
            if(size=="2030"){
                switch (colors_quantity) {
                  case 1:
                    pricePerItem=$row.find('td').eq(7).html();
                    break;
                  case 2:
                    pricePerItem=$row.find('td').eq(8).html();
                    break;
                  case 3:
                    pricePerItem=$row.find('td').eq(9).html();
                    break;
                  default:
                  	pricePerItem=parseFloat($row.find('td').eq(9).html())+(colors_quantity-3)*4;
                  	console.log($row.find('td').eq(9).html());
                  	console.log(parseFloat($row.find('td').eq(9).html()));  
                }
            }

            // Пакеты 30х40
            if(size=="3040"){
                switch (colors_quantity) {
                  case 1:
                    pricePerItem=$row.find('td').eq(4).html();
                    break;
                  case 2:
                    pricePerItem=$row.find('td').eq(5).html();
                    break;
                  case 3:
                    pricePerItem=$row.find('td').eq(6).html();
                    break;
                  default:
                  	pricePerItem=parseFloat($row.find('td').eq(6).html())+(colors_quantity-3)*4;  
                }
            }

            // Пакеты 40х50
            if(size=="4050"){
                switch (colors_quantity) {
                  case 1:
                    pricePerItem=$row.find('td').eq(1).html();
                    break;
                  case 2:
                    pricePerItem=$row.find('td').eq(2).html();
                    break;
                  case 3:
                    pricePerItem=$row.find('td').eq(3).html();
                    break;
                  default:
                  	pricePerItem=parseFloat($row.find('td').eq(3).html())+(colors_quantity-3)*4;  
                }

            }

            var additionalK=[];


            // Пакеты "Другие"
            if(size=="9999"){
            	 switch (colors_quantity) {
                  case 1:
                    pricePerItem=$row.find('td').eq(1).html();
                    break;
                  case 2:
                    pricePerItem=$row.find('td').eq(2).html();
                    break;
                  case 3:
                    pricePerItem=$row.find('td').eq(3).html();
                    break;
                  default:
                  	pricePerItem=parseFloat($row.find('td').eq(3).html())+(colors_quantity-3)*4;  
                }

                additionalK.push(1.2);
            }



            
            // Надбавка за цвет золото
            if($('input[name="isGold"]').is(':checked')){
            	additionalK.push(1.2);
            }

            // Надбавка за плотность
            switch (thick) {
                  case 70:
                    additionalK.push(1.2);
                    break;
                  case 80:
                    additionalK.push(1.25);
                    break;
                  case 100:
                    additionalK.push(1.30);
                    break;
                  default:
                  
            }

            // Надбавка за цвет пакета
            if (color=="ce9e49") {
                additionalK.push(1.3)
            }

            if (color=="D8D8D8") {
                additionalK.push(1.3)
            }

            if (color!="D8D8D8" && color!="ce9e49" && color!="white" && color!="FFFFFF") {
                additionalK.push(1.2)
            }           

            console.log("Надбавки: "+additionalK);
            additionalK.sort();
            console.log("Надбавки отсортированы: "+additionalK);
            console.log("Стоимость 1шт до надбавок: "+pricePerItem);

            pricePerItem=parseFloat(pricePerItem);
            for (var x in additionalK) {
            	console.log('-- price * '+additionalK[x]);
  				pricePerItem=pricePerItem*additionalK[x];
			}
			pricePerItem=pricePerItem.toFixed(2);
            console.log("Стоимость 1шт с надбавками и округлением до 2х знаков: "+pricePerItem);

            $('.s-cost-per-item').html(pricePerItem+" РУБ");
            $('.s-cost-total').html((pricePerItem*quantity).toFixed(2)+" РУБ");


        }

        getCurrentValues();
        showCurrentValues();
        getPriceByCurrentValues();


    $('.js-exec-calc').click(function(){
      getCurrentValues();
        showCurrentValues();
        getPriceByCurrentValues();	
    });

    	/* Отправка данных из калькулятора */
    $('.js-send-calc').click(function(){

    	var $form=$('.calc-query-form');
    	var scErrors=false;
    	$form.find('.required').removeClass('error');

    	$form.find('.required').each(function(){
    		if($(this).val()==""){
    			$(this).addClass('error');
    			scErrors=true;
    	}
    	});
    	if(!scErrors){

    			quantity=$('.s-calculator-quantity .select-box-current').attr('value');
		        size=$('input[name="bag-size"]:checked').val();
		        color=$('.s-calculator-color .select-box-current').html();
		        thick=parseInt($('.s-calculator-thick .select-box-current').attr('value'));
		        colors_quantity=parseInt($('.s-calculator-colors .select-box-current').attr('value'));
		        shape=$('.s-calculator-shape .select-box-current').html();
		        
		        if($('input[name="isGold"]').is(':checked')){
		        	isGold=1;
		        }else{
		        	isGold=0;
		        }

				pricePerItem=$('.s-cost-per-item').html();
            	priceTotal=$('.s-cost-total').html();

            	$.get("/send-calc.php",{
            		iquantity: quantity,
            		isize: size,
            		icolor: color,
            		ithick: thick,
            		icolors_quantity: colors_quantity,
            		iisGold: isGold,
            		ipricePerItem: pricePerItem,
            		ipriceTotal: priceTotal,
            		ishape: shape,
            		iname: $form.find('input[name="calcUserName"]').val(),
            		imail: $form.find('input[name="calcUserEmail"]').val(),
            		itype: $form.find('input[name="calcUserType"]').val()
            	},function(data){
            		console.log(data);
            		if(data==1){	
            			$form.find('input[name="calcUserName"], input[name="calcUserEmail"]').val('');
            			$form.find('.form-success').fadeIn();
            		}
            	});
    		
    	}


    });


    	/* Select box*/
	$('.js-set-select-box-option').click(function(){
		var tmpVal=$(this).attr('value');
		var inner=$(this).html();

		var currentEl=$(this).parent().parent().find('.select-box-current');
		currentEl.attr('value',tmpVal).html(inner);
		currentEl.removeClass('active');

			      	getCurrentValues();
                    showCurrentValues();
                    getPriceByCurrentValues();

	});


	/* Hover таблиц*/
	$('.print-type-table table td').mouseover(function(){
		var el=$(this);
		var num = $(this).parent().find('td').index(el);
		//console.log(num);
		//el.parent().parent().find('tr:eq(0) td:eq('+num+')').addClass('hover')
		el.parent().find('td:eq(0)').addClass('hover');
	}).mouseout(function(){
		var el=$(this);
		var num = $(this).parent().find('td').index(el);
		//console.log(num);
		//el.parent().parent().find('tr:eq(0) td:eq('+num+')').removeClass('hover')
		el.parent().find('td:eq(0)').removeClass('hover');		
	});

	/* Ajax Cart */
	$('#cart > .heading a').live('click', function() {
		$('#cart').addClass('active');
		
		$('#cart').load('index.php?route=module/cart #cart > *');
		
		$('#cart').live('mouseleave', function() {
			$(this).removeClass('active');
		});
	});
	
	/* Mega Menu */
	$('#menu ul > li > a + div').each(function(index, element) {
		// IE6 & IE7 Fixes
		if ($.browser.msie && ($.browser.version == 7 || $.browser.version == 6)) {
			var category = $(element).find('a');
			var columns = $(element).find('ul').length;
			
			$(element).css('width', (columns * 143) + 'px');
			$(element).find('ul').css('float', 'left');
		}		
		
		var menu = $('#menu').offset();
		var dropdown = $(this).parent().offset();
		
		i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());
		
		if (i > 0) {
			$(this).css('margin-left', '-' + (i + 5) + 'px');
		}
	});

	// IE6 & IE7 Fixes
	if ($.browser.msie) {
		if ($.browser.version <= 6) {
			$('#column-left + #column-right + #content, #column-left + #content').css('margin-left', '195px');
			
			$('#column-right + #content').css('margin-right', '195px');
		
			$('.box-category ul li a.active + ul').css('display', 'block');	
		}
		
		if ($.browser.version <= 7) {
			$('#menu > ul > li').bind('mouseover', function() {
				$(this).addClass('active');
			});
				
			$('#menu > ul > li').bind('mouseout', function() {
				$(this).removeClass('active');
			});	
		}
	}
	
	$('.success img, .warning img, .attention img, .information img').live('click', function() {
		$(this).parent().fadeOut('slow', function() {
			$(this).remove();
		});
	});	
});

function getURLVar(key) {
	var value = [];
	
	var query = String(document.location).split('?');
	
	if (query[1]) {
		var part = query[1].split('&');

		for (i = 0; i < part.length; i++) {
			var data = part[i].split('=');
			
			if (data[0] && data[1]) {
				value[data[0]] = data[1];
			}
		}
		
		if (value[key]) {
			return value[key];
		} else {
			return '';
		}
	}
} 

function addToCart(product_id, quantity) {
	quantity = typeof(quantity) != 'undefined' ? quantity : 1;

	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: 'product_id=' + product_id + '&quantity=' + quantity,
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, .information, .error').remove();
			
			if (json['redirect']) {
				location = json['redirect'];
			}
			
			if (json['success']) {
				$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
				
				$('.success').fadeIn('slow');
				
				$('#cart-total').html(json['total']);

			}	
		}
	});
}
function addToWishList(product_id) {
	$.ajax({
		url: 'index.php?route=account/wishlist/add',
		type: 'post',
		data: 'product_id=' + product_id,
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, .information').remove();
						
			if (json['success']) {
				$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
				
				$('.success').fadeIn('slow');
				
				$('#wishlist-total').html(json['total']);
				
				$('html, body').animate({ scrollTop: 0 }, 'slow');
			}	
		}
	});
}

function addToCompare(product_id) { 
	$.ajax({
		url: 'index.php?route=product/compare/add',
		type: 'post',
		data: 'product_id=' + product_id,
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, .information').remove();
						
			if (json['success']) {
				$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
				
				$('.success').fadeIn('slow');
				
				$('#compare-total').html(json['total']);
				
				$('html, body').animate({ scrollTop: 0 }, 'slow'); 
			}	
		}
	});
}