<?php echo $header; ?>

  <div class="catalog_header catalog_header-category">
    <h1><?php echo $heading_title; ?></h1>
    <div class="breadcrumb">
      <?php $i=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php if($i<sizeof($breadcrumbs)-1){ ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php }else{ ?>
        <?php echo $breadcrumb['separator']; ?><?php echo $breadcrumb['text']; ?>
        <?php } ?>
      <?php $i++; } ?>
    </div>
  </div>


<?php echo $column_left; ?><?php echo $column_right; ?>

  <?php if (!isset($news_info)) { ?>
  <div class="container container-account">
<script type="text/javascript">
    var feed = new Instafeed({
        get: 'user',
        userId: '1268766989',
    limit:15,
    accessToken: '1268766989.1677ed0.11fe9b83970149db858c4680c31ea7d2',
    resolution: 'standard_resolution',
        template: '<li><a href="{{link}}" target=_blank><img src="{{image}}" /></a></li>',
    after: function(){
      $('#instafeed').bxSlider({
        pager:false,
        minSlides:5,
        maxSlides:5,
        slideWidth:175,
        slideMargin:27,
        moveSlides:1,
        nextSelector:'#goRight',
        prevSelector:'#goLeft'
      });
    }

    });
    feed.run();
</script>

<div id="instagram" class="box box-clean box-block" style="height:238px;">
  <div class="box-heading">Фотопоток <a href="https://www.instagram.com/volgoprint/" target="_blank" class="module-link instagram-external">Мы в Instagram</a></div>
  <div id="instafeed" class="instafeed"></div>
  <div class="instagram-navigation instagram-navigation_goLeft" id="goLeft"></div>
  <div class="instagram-navigation instagram-navigation_goRight" id="goRight"></div>
</div>
</div>

<?php } ?>



  <div class="container container-newspage">



  <?php if (isset($news_info)) { ?>
    <div class="full-news" <?php if ($image) { echo 'style="min-height: ' . $min_height . 'px;"'; } ?>>
      <?php if ($image) { ?>
        <img align="left" style="border: none; margin-right: 20px;" src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
      <?php } ?>
      <?php echo $description; ?>
    </div>

  <?php } elseif (isset($news_data)) { ?>

    <?php foreach ($news_data as $news) { ?>


      <div class="box-news">
                <div class="box-image"><a href="<?php echo $news['href']; ?>"><? if($news['image']!=""){ echo "<img src='".$news['image']."' alt='".$news['title']."'/>"; }?></a></div>
                  <div class="box-text">
                    <a href="<?php echo $news['href']; ?>"><h4><?php echo $news['title']; ?></h4></a>
                    <a href="<?php echo $news['href']; ?>"><?php echo $news['description']; ?></a>
                  </div>


      </div>
    <?php } ?>

  <div class="pagination"><?php echo $pagination; ?></div>
  <?php } ?>
</div>
<?php echo $footer; ?>
