<?php echo $header; ?>
<div class="catalog_header catalog_header-category">
    <h1><?php echo $heading_title; ?></h1>
    <div class="breadcrumb">
        <?php $i=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php if($i<sizeof($breadcrumbs)-1){ ?>
        <?php echo $breadcrumb['separator']; ?><a
                href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php }else{ ?>
        <?php echo $breadcrumb['separator']; ?><?php echo $breadcrumb['text']; ?>
        <?php } ?>
        <?php $i++; } ?>
    </div>
</div>

<?php echo $column_left; ?><?php echo $column_right; ?>

<div class="container container-information">
    <div id="content">
        <?php echo $content_top; ?>

        <div class="print-type-holder">

            <span class="print-type-title js-toggle-active icon-shirt">Печать на футболках</span>
            <div class="print-type-table">
                <h2>Печать на белых футболках (шелкография)</h2>
                <table>
                    <tr>
                        <td>Тираж</td>
                        <td>1 цвет</td>
                        <td>2 цвета</td>
                        <td>3 цвета</td>
                        <td>4 цвета</td>
                    </tr>
                    <tr>
                        <td>20</td>
                        <td>55</td>
                        <td>110</td>
                        <td>150</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>30</td>
                        <td>47</td>
                        <td>95</td>
                        <td>129</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>50</td>
                        <td>33</td>
                        <td>50</td>
                        <td>75</td>
                        <td>180</td>
                    </tr>
                    <tr>
                        <td>100</td>
                        <td>28</td>
                        <td>49</td>
                        <td>73</td>
                        <td>140</td>
                    </tr>
                    <tr>
                        <td>200</td>
                        <td>25</td>
                        <td>39</td>
                        <td>45</td>
                        <td>140</td>
                    </tr>
                    <tr>
                        <td>300</td>
                        <td>21</td>
                        <td>33</td>
                        <td>43</td>
                        <td>140</td>
                    </tr>
                    <tr>
                        <td>500</td>
                        <td>20</td>
                        <td>30</td>
                        <td>38</td>
                        <td>70</td>
                    </tr>
                    <tr>
                        <td>1000</td>
                        <td>18</td>
                        <td>27</td>
                        <td>35</td>
                        <td>50</td>
                    </tr>

                </table>

                <p class="print-table-description">Изготовление формы - 500 рублей за каждый цвет<br/>
                    Краска золото, серебро +20%<br/>
                    Формат печати А3 +20%<br/>
                    Вспененная краска (объемная) +20%</p>

                <h2>Печать на цветных футболках, толстовках, бейсболках, сумках (шелкография)</h2>
                <table>
                    <tr>
                        <td>Тираж</td>
                        <td>1 цвет</td>
                        <td>2 цвета</td>
                        <td>3 цвета</td>
                        <td>4 цвета</td>
                    </tr>
                    <tr>
                        <td>20</td>
                        <td>68</td>
                        <td>148</td>
                        <td>185</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>30</td>
                        <td>57</td>
                        <td>114</td>
                        <td>155</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>50</td>
                        <td>39</td>
                        <td>59</td>
                        <td>89</td>
                        <td>230</td>
                    </tr>
                    <tr>
                        <td>100</td>
                        <td>32</td>
                        <td>57</td>
                        <td>86</td>
                        <td>160</td>
                    </tr>
                    <tr>
                        <td>200</td>
                        <td>31</td>
                        <td>39</td>
                        <td>45</td>
                        <td>160</td>
                    </tr>
                    <tr>
                        <td>300</td>
                        <td>24</td>
                        <td>37</td>
                        <td>49</td>
                        <td>160</td>
                    </tr>
                    <tr>
                        <td>500</td>
                        <td>22</td>
                        <td>33</td>
                        <td>43</td>
                        <td>90</td>
                    </tr>
                    <tr>
                        <td>1000</td>
                        <td>20</td>
                        <td>31</td>
                        <td>40</td>
                        <td>60</td>
                    </tr>
                </table>

                <p class="print-table-description">Изготовление формы - 500 рублей за каждый цвет<br/>
                    Краска золото, серебро +20%<br/>
                    Формат печати А3 +20%<br/>
                    Вспененная краска (объемная) +20%</p>

                <h2>Сигнальный образец футболки или др изделия с шелкографией</h2>
                <table>
                    <tr>
                        <td>1 цвет</td>
                        <td>2 цвета</td>
                        <td>3 цвета</td>
                        <td>4 цвета</td>
                    </tr>
                    <tr>
                        <td>1500 + стоимость изделия</td>
                        <td>2000 + стоимость изделия</td>
                        <td>3000 + стоимость изделия</td>
                        <td>4000 + стоимость изделия</td>
                    </tr>
                </table>

                <h2>Печать на спецодежде, сумках зонтах</h2>
                <table>
                    <tr>
                        <td>Тираж</td>
                        <td>10</td>
                        <td>20</td>
                        <td>30</td>
                        <td>50</td>
                        <td>100</td>
                    </tr>
                    <tr>
                        <td>1 цвет</td>
                        <td>140</td>
                        <td>120</td>
                        <td>105</td>
                        <td>85</td>
                        <td>75</td>
                    </tr>
                </table>
                <p class="print-table-description">Стоимость печати за единицу в 1 цвет за одно место печати<br/>Изготовление
                    формы - 500 рублей за каждый цвет<br/>За каждый дополнительный цвет +40% к стоимости печати за
                    единицу<br/>Светоотражающаяся краска +20% к стоимости</p>

                <h2>Прямая печать, термотрансфер, сублимация</h2>
                <table>
                    <tr>
                        <td>Формат А3</td>
                        <td>Формат А4</td>
                        <td>Формат А5</td>
                    </tr>
                    <tr>
                        <td>400</td>
                        <td>200</td>
                        <td>150</td>
                    </tr>
                </table>
            </div>

            <span class="print-type-title js-toggle-active icon-pen">Тампопечать</span>
            <div class="print-type-table">

                <h2>На ручках</h2>
                <table>
                    <tr>
                        <td>Тираж</td>
                        <td>100</td>
                        <td>200</td>
                        <td>300</td>
                        <td>400</td>
                        <td>500</td>
                    </tr>
                    <tr>
                        <td>1 цвет</td>
                        <td>5,80</td>
                        <td>5,30</td>
                        <td>4,70</td>
                        <td>4,20</td>
                        <td>3,80</td>
                    </tr>
                    <tr>
                        <td>2 цвета</td>
                        <td>6,95</td>
                        <td>6,95</td>
                        <td>5,95</td>
                        <td>5,95</td>
                        <td>5,40</td>
                    </tr>
                    <tr>
                        <td>-</td>
                        <td class="hightlight">1000</td>
                        <td class="hightlight">3000</td>
                        <td class="hightlight">5000</td>
                        <td class="hightlight">10000</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>1 цвет</td>
                        <td>3,70</td>
                        <td>2,80</td>
                        <td>2,00</td>
                        <td>1,80</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>2 цвета</td>
                        <td>5,4</td>
                        <td>4,35</td>
                        <td>3,75</td>
                        <td>2,8</td>
                        <td>-</td>
                    </tr>
                </table>

                <p class="print-table-description">
                    Изготовление формы - 500 рублей за каждый цвет<br/>
                    Минимальный заказ - 1600 рублей (1 цвет), 2550 рублей (2 цвета)<br/>
                    Печать золотом или серебром +20%
                </p>

                <h2>На сувенирах</h2>
                <table>
                    <tr>
                        <td>Тираж</td>
                        <td>100</td>
                        <td>200</td>
                        <td>300</td>
                        <td>400</td>
                        <td>500</td>
                    </tr>
                    <tr>
                        <td>1 цвет</td>
                        <td>13,2</td>
                        <td>10,7</td>
                        <td>9,3</td>
                        <td>8,5</td>
                        <td>7,7</td>
                    </tr>
                    <tr>
                        <td>2 цвета</td>
                        <td>16,12</td>
                        <td>15,9</td>
                        <td>13,5</td>
                        <td>13,5</td>
                        <td>10,4</td>
                    </tr>
                    <tr>
                        <td>-</td>
                        <td class="hightlight">1000</td>
                        <td class="hightlight">3000</td>
                        <td class="hightlight">5000</td>
                        <td class="hightlight">10000</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>1 цвет</td>
                        <td>7,1</td>
                        <td>6,1</td>
                        <td>4,5</td>
                        <td>3,9</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>2 цвета</td>
                        <td>11,8</td>
                        <td>9,9</td>
                        <td>8,6</td>
                        <td>6,4</td>
                        <td>-</td>
                    </tr>
                </table>

                <p class="print-table-description">
                    Изготовление формы - 500 рублей за каждый цвет<br/>
                    Минимальный заказ - 2600 рублей (1 цвет), 3850 рублей (2 цвета)<br/>
                    Печать золотом или серебром +20%
                </p>


            </div>

            <span class="print-type-title js-toggle-active icon-umbrella">УФ печать</span>
            <div class="print-type-table">
                <h2>Стоимость УФ печати на ручках</h2>
                <table>
                    <tr>
                        <td>Тираж</td>
                        <td>100</td>
                        <td>300</td>
                        <td>500</td>
                        <td>1000 и более</td>
                    </tr>
                    <tr>
                        <td>руб/шт</td>
                        <td>18</td>
                        <td>17</td>
                        <td>16</td>
                        <td>15</td>
                    </tr>
                </table>

                <h2>Стоимость УФ печати на сувенирах</h2>
                <h4>Начальные расходы: 550.00</h4>
                <table>
                    <tr>
                        <td>Тираж</td>
                        <td>до 10см<sup>2</sup></td>
                        <td>за каждый см<sup>2</sup> сверх 10</td>
                    </tr>
                    <tr>
                        <td>1-9</td>
                        <td>33</td>
                        <td>+0,5</td>
                    </tr>
                    <tr>
                        <td>10-49</td>
                        <td>28</td>
                        <td>+0,5</td>
                    </tr>
                    <tr>
                        <td>50-99</td>
                        <td>25</td>
                        <td>+0,5</td>
                    </tr>
                    <tr>
                        <td>100-299</td>
                        <td>20</td>
                        <td>+0,5</td>
                    </tr>
                    <tr>
                        <td>300-499</td>
                        <td>18</td>
                        <td>+0,5</td>
                    </tr>
                    <tr>
                        <td>от 500</td>
                        <td>16</td>
                        <td>+0,5</td>
                    </tr>
                </table>

                <p class="print-table-description">Минимальный заказ на нанесение 2000 рублей
                </p>

                <h2>Круговая УФ печати</h2>
                <h4>Начальные расходы: 825.00</h4>
                <table>
                    <tr>
                        <td>Тираж</td>
                        <td>до 100см<sup>2</sup></td>
                        <td>за каждый см<sup>2</sup> сверх 100</td>
                    </tr>
                    <tr>
                        <td>1-9</td>
                        <td>165</td>
                        <td>+1,1</td>
                    </tr>
                    <tr>
                        <td>10-19</td>
                        <td>137</td>
                        <td>+1,1</td>
                    </tr>
                    <tr>
                        <td>20-49</td>
                        <td>124</td>
                        <td>+1,1</td>
                    </tr>
                    <tr>
                        <td>50-99</td>
                        <td>119</td>
                        <td>+1,1</td>
                    </tr>
                    <tr>
                        <td>от 100</td>
                        <td>110</td>
                        <td>+1,1</td>
                    </tr>
                </table>

                <p class="print-table-description">Минимальный заказ на нанесение 2000 рублей
                </p>
            </div>

            <span class="print-type-title js-toggle-active icon-paket">Пакеты полиэтиленовые с вырубной ручкой</span>
            <div class="print-type-table">
                <div class="print-calculator">

                    <div class="print-calculator-row">
                        <label class="print-row-label-title">Размер пакета:</label>
                        <label>
                        <input class="size-radio js-exec-calc" type="radio" name="bag-size" value="4050" checked/><span
                                class="size-label" value="4050">40x50см</span>
                        </label>
                        <label>
                        <input class="size-radio js-exec-calc" type="radio" name="bag-size" value="3040"/><span
                                class="size-label" value="3040">30x40см</span>
                        </label>
                        <label>
                        <input class="size-radio js-exec-calc" type="radio" name="bag-size" value="2030"/><span
                                class="size-label" value="2030">20x30см</span>
                        </label>
                        <label>
                        <input class="size-radio js-exec-calc" type="radio" name="bag-size" value="9999"/><span
                                class="size-label">50х60 и 60х50</span>
                        </label>
                    </div>

                    <div class="print-calculator-row pcr-2">

                        <div class="col-3">
                            <span class="calc-spans">Тираж:</span>
                            <div class="select-box s-calculator-quantity js-exec-calc">
                                <div class="select-box-current js-toggle-active" value="100">100</div>
                                <div class="select-box-option-holder">
                                    <div class="select-box-option js-set-select-box-option" value='100'>100</div>
                                    <div class="select-box-option js-set-select-box-option" value='200'>200</div>
                                    <div class="select-box-option js-set-select-box-option" value='300'>300</div>
                                    <div class="select-box-option js-set-select-box-option" value='500'>500</div>
                                    <div class="select-box-option js-set-select-box-option" value='1000'>1000</div>
                                    <div class="select-box-option js-set-select-box-option" value='1500'>1500</div>
                                    <div class="select-box-option js-set-select-box-option" value='2000'>2000</div>
                                    <div class="select-box-option js-set-select-box-option" value='3000'>3000</div>
                                    <div class="select-box-option js-set-select-box-option" value='5000'>5000</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-3">
                            <span class="calc-spans">Цвет:</span>
                            <div class="select-box s-calculator-color js-exec-calc">
                                <div class="select-box-current js-toggle-active" value="white">Белый</div>
                                <div class="select-box-option-holder">
                                    <div class="select-box-option js-set-select-box-option" data-color="#FFFFFF"
                                         value='FFFFFF'>Белый
                                    </div>
                                    <div class="select-box-option js-set-select-box-option" data-color="#337b7a"
                                         value='337b7a'>Бирюзовый
                                    </div>
                                    <div class="select-box-option js-set-select-box-option" data-color="#CC0000"
                                         value='CC0000'>Бордовый
                                    </div>
                                    <div class="select-box-option js-set-select-box-option" data-color="#00CCFF"
                                         value='00CCFF'>Голубой мет.
                                    </div>
                                    <div class="select-box-option js-set-select-box-option" data-color="#4b9dcd"
                                         value='4b9dcd'>Голубой
                                    </div>
                                    <div class="select-box-option js-set-select-box-option" data-color="#fff71a"
                                         value='fff71a'>Жёлтый
                                    </div>
                                    <div class="select-box-option js-set-select-box-option" data-color="#2c8c4c"
                                         value='2c8c4c'>Зеленый
                                    </div>
                                    <div class="select-box-option js-set-select-box-option" data-color="#ce9e49"
                                         value='ce9e49'>Золото
                                    </div>
                                    <div class="select-box-option js-set-select-box-option" data-color="#6b3d2d"
                                         value='6b3d2d'>Коричневый
                                    </div>
                                    <div class="select-box-option js-set-select-box-option" data-color="#EF0504"
                                         value='EF0504'>Красный
                                    </div>
                                    <div class="select-box-option js-set-select-box-option" data-color="#FF6600"
                                         value='FF6600'>Оранжевый
                                    </div>
                                    <div class="select-box-option js-set-select-box-option" data-color="#f496b0"
                                         value='f496b0'>Розовый
                                    </div>
                                    <div class="select-box-option js-set-select-box-option" data-color="#62af3f"
                                         value='62af3f'>Салатовый
                                    </div>
                                    <div class="select-box-option js-set-select-box-option" data-color="#FFCC33"
                                         value='FFCC33'>Светло-беж.
                                    </div>
                                    <div class="select-box-option js-set-select-box-option" data-color="#D8D8D8"
                                         value='D8D8D8'>Серебро
                                    </div>
                                    <div class="select-box-option js-set-select-box-option" data-color="#0000FF"
                                         value='0000FF'>Синий
                                    </div>
                                    <div class="select-box-option js-set-select-box-option" data-color="#009900"
                                         value='009900'>Тёмно-зеленый
                                    </div>
                                    <div class="select-box-option js-set-select-box-option" data-color="#0596AF"
                                         value='0596AF'>Тёмно-синий
                                    </div>
                                    <div class="select-box-option js-set-select-box-option" data-color="#8B00FF"
                                         value='8B00FF'>Фиолетовый
                                    </div>
                                    <div class="select-box-option js-set-select-box-option" data-color="#000000"
                                         value='000000'>Черный
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-3">
                            <span class="calc-spans">Плотность пакета:</span>
                            <div class="select-box s-calculator-thick js-exec-calc">
                                <div class="select-box-current js-toggle-active" value="50">50 мкм</div>
                                <div class="select-box-option-holder">
                                    <div class="select-box-option js-set-select-box-option" value='50'>50 мкм</div>
                                    <div class="select-box-option js-set-select-box-option" value='70'>70 мкм</div>
                                    <div class="select-box-option js-set-select-box-option" value='80'>80 мкм</div>
                                    <div class="select-box-option js-set-select-box-option" value='100'>100 мкм</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-3">
                            <span class="calc-spans">Кол-во цветов:</span>
                            <div class="select-box s-calculator-colors js-exec-calc">
                                <div class="select-box-current js-toggle-active" value="1">1</div>
                                <div class="select-box-option-holder">
                                    <div class="select-box-option js-set-select-box-option" value='1'>1</div>
                                    <div class="select-box-option js-set-select-box-option" value='2'>2</div>
                                    <div class="select-box-option js-set-select-box-option" value='3'>3</div>
                                    <div class="select-box-option js-set-select-box-option" value='4'>4</div>
                                    <div class="select-box-option js-set-select-box-option" value='5'>5</div>
                                    <div class="select-box-option js-set-select-box-option" value='6'>6</div>
                                    <div class="select-box-option js-set-select-box-option" value='7'>7</div>
                                    <div class="select-box-option js-set-select-box-option" value='8'>8</div>
                                    <div class="select-box-option js-set-select-box-option" value='9'>9</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="print-calculator-row">
                        <input class="js-exec-calc" type="checkbox" name="isGold"><span>Краска золото, серебро</span>
                        <span class="align-right">штука: <b class="s-cost-per-item">0 руб</b></span>
                        <span class="align-right">тираж: <b class="s-cost-total">0 руб</b></span>
                    </div>
                    <form class="calc-query-form">
                        <div class="print-calculator-row top-separator">
                            <label class="print-row-label-title">Имя:</label>
                            <input type="text" class="input-box required" name="calcUserName"
                                   placeholder="Иванов Иван"/>
                        </div>
                        <div class="print-calculator-row top-separator">
                            <label class="print-row-label-title">E-mail/Телефон:</label>
                            <input type="text" class="input-box required" name="calcUserEmail"
                                   placeholder="hello@mail.ru"/>
                        </div>
                        <div class="print-calculator-row top-separator">
                            <input type="button" class="js-send-calc input-button" value="Отправить заявку"/>
                        </div>

                        <div class="print-calculator-row top-separator form-success">
                            <h2>Спасибо, ваша заявка успешно отправлена</h2>
                        </div>
                    </form>


                </div>
                <table id="dataBags" style="display:none">
                    <tr>
                        <td rowspan="2">Тираж, шт</td>
                        <td colspan="3">Размер пакета 40х50</td>
                        <td colspan="3">Размер пакета 30х40</td>
                        <td colspan="3">Размер пакета 20х30</td>
                    </tr>
                    <tr>
                        <td>1 цвет</td>
                        <td>2 цвета</td>
                        <td>3 цвета</td>
                        <td>1 цвет</td>
                        <td>2 цвета</td>
                        <td>3 цвета</td>
                        <td>1 цвет</td>
                        <td>2 цвета</td>
                        <td>3 цвета</td>
                    </tr>
                    <tr data-quantity="100">
                        <td>100</td>
                        <td>17.2</td>
                        <td>23.3</td>
                        <td>31.5</td>
                        <td>16.5</td>
                        <td>22.3</td>
                        <td>30.2</td>
                        <td>16.4</td>
                        <td>22.2</td>
                        <td>30.0</td>
                    </tr>
                    <tr data-quantity="200">
                        <td>200</td>
                        <td>15.2</td>
                        <td>20.6</td>
                        <td>27.9</td>
                        <td>14.5</td>
                        <td>19.6</td>
                        <td>27.5</td>
                        <td>14.4</td>
                        <td>19.5</td>
                        <td>26.4</td>
                    </tr>
                    <tr data-quantity="300">
                        <td>300</td>
                        <td>14.5</td>
                        <td>19.6</td>
                        <td>26.5</td>
                        <td>13.4</td>
                        <td>18.1</td>
                        <td>24.5</td>
                        <td>13.3</td>
                        <td>18.0</td>
                        <td>24.3</td>
                    </tr>
                    <tr data-quantity="500">
                        <td>500</td>
                        <td>13.4</td>
                        <td>18.1</td>
                        <td>24.5</td>
                        <td>11.5</td>
                        <td>15.6</td>
                        <td>21.1</td>
                        <td>11.4</td>
                        <td>15.4</td>
                        <td>20.8</td>
                    </tr>
                    <tr data-quantity="1000">
                        <td>1000</td>
                        <td>10.4</td>
                        <td>14.1</td>
                        <td>19.1</td>
                        <td>8.7</td>
                        <td>11.8</td>
                        <td>16.0</td>
                        <td>8.6</td>
                        <td>11.7</td>
                        <td>15.8</td>
                    </tr>
                    <tr data-quantity="1500">
                        <td>1500</td>
                        <td>9.7</td>
                        <td>13.1</td>
                        <td>17.7</td>
                        <td>8.1</td>
                        <td>11.0</td>
                        <td>14.9</td>
                        <td>8.0</td>
                        <td>10.8</td>
                        <td>14.5</td>
                    </tr>
                    <tr data-quantity="2000">
                        <td>2000</td>
                        <td>9.0</td>
                        <td>12.2</td>
                        <td>16.5</td>
                        <td>7.5</td>
                        <td>10.2</td>
                        <td>13.8</td>
                        <td>7.4</td>
                        <td>10.0</td>
                        <td>13.5</td>
                    </tr>
                    <tr data-quantity="3000">
                        <td>3000</td>
                        <td>8.8</td>
                        <td>11.9</td>
                        <td>16.1</td>
                        <td>7.3</td>
                        <td>9.9</td>
                        <td>13.4</td>
                        <td>7.0</td>
                        <td>9.5</td>
                        <td>12.9</td>
                    </tr>
                    <tr data-quantity="5000">
                        <td>5000</td>
                        <td>8.6</td>
                        <td>11.7</td>
                        <td>15.8</td>
                        <td>7.1</td>
                        <td>9.6</td>
                        <td>13.0</td>
                        <td>6.8</td>
                        <td>9.2</td>
                        <td>12.5</td>
                    </tr>
                </table>
                <p class="print-table-description">
                    Белые полиэтиленовые пакеты (ПВД) 40х50 и 30х40 тиражом от 100 штук.
                </p>

            </div>

            <span class="print-type-title js-toggle-active icon-paket">Бумажные пакеты</span>
            <div class="print-type-table">
                <table>
                    <tr>
                        <td>Тираж</td>
                        <td>25х35х8(10)</td>
                        <td>30х40х10(12)</td>
                        <td>39х50х8</td>
                    </tr>
                    <tr>
                        <td>100</td>
                        <td>194</td>
                        <td>220</td>
                        <td>235</td>
                    </tr>
                    <tr>
                        <td>200</td>
                        <td>136</td>
                        <td>160</td>
                        <td>171</td>
                    </tr>
                    <tr>
                        <td>300</td>
                        <td>109</td>
                        <td>138</td>
                        <td>150</td>
                    </tr>
                    <tr>
                        <td>500</td>
                        <td>87</td>
                        <td>120</td>
                        <td>133</td>
                    </tr>
                    <tr>
                        <td>1000</td>
                        <td>77</td>
                        <td>88</td>
                        <td>101</td>
                    </tr>
                </table>
                <p class="print-table-description">
                    170гр, 4+4, глянцевая ламинация
                </p>

            </div>

            <span class="print-type-title js-toggle-active icon-paket">Пакеты полиэтиленовые майки</span>
            <div class="print-type-table">
                <table>
                    <tr>
                        <td>Размеры/Количество</td>
                        <td>5 000<br/>1-3 цвета</td>
                        <td>5 000<br/>4 цвета</td>
                        <td>10 000<br/>1-3 цвета</td>
                        <td>10 000<br/>4 цвета</td>
                        <td>20 000<br/>1-3 цвета</td>
                        <td>20 000<br/>4 цвета</td>
                        <td>30 000<br/>1-3 цвета</td>
                        <td>30 000<br/>4 цвета</td>
                        <td>50 000<br/>1-3 цвета</td>
                        <td>50 000<br/>4 цвета</td>
                    </tr>
                    <tr>
                        <td>20+10x40/13мкм</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>0,94</td>
                        <td>1,04</td>
                        <td>0,85</td>
                        <td>0,94</td>
                    </tr>
                    <tr>
                        <td>20+10x40/15мкм</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>1,08</td>
                        <td>1,16</td>
                        <td>0,97</td>
                        <td>1,06</td>
                    </tr>
                    <tr>
                        <td>28+14x50/15мкм</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>1,67</td>
                        <td>1,76</td>
                        <td>1,58</td>
                        <td>1,68</td>
                        <td>1,42</td>
                        <td>1,50</td>
                    </tr>
                    <tr>
                        <td>28+14x50/18мкм</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>1,97</td>
                        <td>2,05</td>
                        <td>1,88</td>
                        <td>1,97</td>
                        <td>1,69</td>
                        <td>1,78</td>
                    </tr>
                    <tr>
                        <td>30+15x60/15мкм</td>
                        <td>-</td>
                        <td>-</td>
                        <td>2,50</td>
                        <td>2,58</td>
                        <td>2,09</td>
                        <td>2,17</td>
                        <td>1,92</td>
                        <td>2,00</td>
                        <td>1,78</td>
                        <td>1,86</td>
                    </tr>
                    <tr>
                        <td>30+15x60/20мкм</td>
                        <td>-</td>
                        <td>-</td>
                        <td>3,05</td>
                        <td>3,25</td>
                        <td>2,65</td>
                        <td>2,74</td>
                        <td>2,48</td>
                        <td>2,57</td>
                        <td>2,34</td>
                        <td>2,42</td>
                    </tr>
                    <tr>
                        <td>38+18x60/16мкм</td>
                        <td>-</td>
                        <td>-</td>
                        <td>3,10</td>
                        <td>3,18</td>
                        <td>3,24</td>
                        <td>3,34</td>
                        <td>2,48</td>
                        <td>2,57</td>
                        <td>2,34</td>
                        <td>2,42</td>
                    </tr>
                    <tr>
                        <td>38+18x60/20мкм</td>
                        <td>-</td>
                        <td>-</td>
                        <td>3,78</td>
                        <td>3,89</td>
                        <td>3,25</td>
                        <td>3,35</td>
                        <td>2,99</td>
                        <td>3,07</td>
                        <td>2,86</td>
                        <td>2,94</td>
                    </tr>
                    <tr>
                        <td>40+20x70/18мкм</td>
                        <td>-</td>
                        <td>-</td>
                        <td>3,98</td>
                        <td>4,07</td>
                        <td>3,62</td>
                        <td>3,73</td>
                        <td>3,49</td>
                        <td>3,49</td>
                        <td>3,12</td>
                        <td>3,20</td>
                    </tr>
                    <tr>
                        <td>40+20x70/25мкм</td>
                        <td>5,92</td>
                        <td>6,05</td>
                        <td>5,33</td>
                        <td>5,41</td>
                        <td>4,74</td>
                        <td>4,74</td>
                        <td>4,60</td>
                        <td>4,60</td>
                        <td>4,15</td>
                        <td>4,24</td>
                    </tr>
                    <tr>
                        <td>44+22x70/25мкм</td>
                        <td>6,37</td>
                        <td>6,46</td>
                        <td>5,71</td>
                        <td>5,80</td>
                        <td>5,05</td>
                        <td>5,14</td>
                        <td>4,72</td>
                        <td>6,48</td>
                        <td>4,46</td>
                        <td>4,55</td>
                    </tr>

                </table>

                <p class="print-table-description">
                    Цены указаны в рублях за 1 шт, белый полиэтилен. Стоимость флексоформ рассчитывается отдельно. Цена
                    может изменяться в зависимости от сложности рисунка, наличия и величины плашки, цвета пакета. Тираж,
                    плотность и размеры пакетов могут быть расчитаны по индивидуальному запросу заказчика, учитывая
                    технические возможности производства.
                </p>


            </div>

            <span class="print-type-title js-toggle-active icon-baloon">Воздушные шары с логотипом</span>
            <div class="print-type-table">
                <h2>12 дюймов (30 см)</h2>
                <table>
                    <tr>
                        <td rowspan="2">Тираж</td>
                        <td colspan="2">12" Пастель</td>
                        <td colspan="2">12" Металлик</td>
                    </tr>
                    <tr>
                        <td>1 цвет, 1 сторона</td>
                        <td>1 цвет, 2 стороны</td>
                        <td>1 цвета, 1 сторона</td>
                        <td>1 цвет, 2 стороны</td>
                    </tr>
                    <tr>
                        <td>100</td>
                        <td>26,00</td>
                        <td>55,00</td>
                        <td>29,4</td>
                        <td>55,7</td>
                    </tr>
                    <tr>
                        <td>200</td>
                        <td>18,07</td>
                        <td>36,3</td>
                        <td>20,06</td>
                        <td>36,9</td>
                    </tr>
                    <tr>
                        <td>300</td>
                        <td>14,77</td>
                        <td>29,4</td>
                        <td>18,1</td>
                        <td>36,2</td>
                    </tr>
                    <tr>
                        <td>500</td>
                        <td>10,77</td>
                        <td>22,8</td>
                        <td>15,36</td>
                        <td>27,67</td>
                    </tr>
                    <tr>
                        <td>1000</td>
                        <td>8,37</td>
                        <td>16,15</td>
                        <td>12,06</td>
                        <td>20,9</td>
                    </tr>
                    <tr>
                        <td>2000</td>
                        <td>8,22</td>
                        <td>15,05</td>
                        <td>11,2</td>
                        <td>19,23</td>
                    </tr>
                    <tr>
                        <td>3000</td>
                        <td>7,777</td>
                        <td>13,73</td>
                        <td>10,08</td>
                        <td>17,03</td>
                    </tr>
                    <tr>
                        <td>5000</td>
                        <td>6,80</td>
                </table>

                <h2>10 дюймов (23см)</h2>
                <table>
                    <tr>
                        <td>Тираж</td>
                        <td>Пастель</td>
                        <td>Металлик</td>
                    </tr>

                    <tr>
                        <td>-</td>
                        <td>1 цвет</td>
                        <td>1 цвет</td>
                    </tr>
                    <tr>
                        <td>100</td>
                        <td>25,00</td>
                        <td>28,4</td>
                    </tr>
                    <tr>
                        <td>200</td>
                        <td>17,07</td>
                        <td>19,06</td>
                    </tr>
                    <tr>
                        <td>300</td>
                        <td>13,77</td>
                        <td>14,1</td>
                    </tr>
                    <tr>
                        <td>500</td>
                        <td>9,77</td>
                        <td>11,36</td>
                    </tr>
                    <tr>
                        <td>1000</td>
                        <td>7,37</td>
                        <td>8,06</td>
                    </tr>
                    <tr>
                        <td>2000</td>
                        <td>6,77</td>
                        <td>7,2</td>
                    </tr>
                    <tr>
                        <td>3000</td>
                        <td>6,17</td>
                        <td>6,08</td>
                    </tr>
                    <tr>
                        <td>5000</td>
                        <td>5,80</td>
                        <td>6</td>
                    </tr>

                </table>
            </div>

            <span class="print-type-title js-toggle-active icon-umbrella">Печать на спецодежде, куртках, зонтах, синтетике, нейлоне</span>
            <div class="print-type-table">
                <table>
                    <tr>
                        <td>Тираж</td>
                        <td>10</td>
                        <td>20</td>
                        <td>30</td>
                        <td>50</td>
                        <td>100</td>
                        <td>500</td>
                        <td>1000</td>
                    </tr>
                    <tr>
                        <td>1 цвет</td>
                        <td>140</td>
                        <td>120</td>
                        <td>105</td>
                        <td>85</td>
                        <td>75</td>
                        <td>50</td>
                        <td>40</td>
                    </tr>
                </table>

                <!--

                <table>
                    <tr>
                        <td>Тираж</td>
                        <td>10</td>
                        <td>20</td>
                        <td>30</td>
                        <td>50</td>
                        <td>100</td>
                    </tr>
                    <tr>
                        <td>1 цвет</td>
                        <td>140</td>
                        <td>91</td>
                        <td>77</td>
                        <td>56</td>
                        <td>31</td>
                    </tr>
                    <tr>
                        <td>2 цвета</td>
                        <td>203</td>
                        <td>185</td>
                        <td>154</td>
                        <td>91</td>
                        <td>46</td>
                    </tr>
                    <tr>
                        <td>3 цвета</td>
                        <td>312</td>
                        <td>272</td>
                        <td>229</td>
                        <td>132</td>
                        <td>69</td>
                    </tr>
                    <tr>
                        <td>4 цвета</td>
                        <td>426</td>
                        <td>368</td>
                        <td>307</td>
                        <td>179</td>
                        <td>91</td>
                    </tr>
                    <tr>
                        <td>-</td>
                        <td class="hightlight">200</td>
                        <td class="hightlight">300</td>
                        <td class="hightlight">500</td>
                        <td class="hightlight">1000</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>1 цвет</td>
                        <td>25</td>
                        <td>22</td>
                        <td>20</td>
                        <td>17</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>2 цвета</td>
                        <td>42</td>
                        <td>39</td>
                        <td>34</td>
                        <td>25</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>3 цвета</td>
                        <td>65</td>
                        <td>62</td>
                        <td>59</td>
                        <td>48</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>4 цвета</td>
                        <td>88</td>
                        <td>85</td>
                        <td>81</td>
                        <td>75</td>
                        <td>-</td>
                    </tr>
                </table> -->

                <p class="print-table-description">Изготовление формы - 500 рублей за каждый цвет<br/>
                    За каждый дополнительный цвет +40% к стоимости за единицу<br/>
                    Светоотражающая краска +20%<br/>
                    Формат печати А3 +20%<br/>
                </p>
            </div>

            <span class="print-type-title js-toggle-active icon-pechat">Шелкография на бумаге, картоне, ежедневниках, папках</span>
            <div class="print-type-table">
                <table>
                    <tr>
                        <td>Тираж</td>
                        <td>50</td>
                        <td>100</td>
                        <td>200</td>
                        <td>300</td>
                        <td>500</td>
                    </tr>
                    <tr>
                        <td>1 цвет</td>
                        <td>21</td>
                        <td>11</td>
                        <td>10</td>
                        <td>9</td>
                        <td>8</td>
                    </tr>
                    <tr>
                        <td>2 цвета</td>
                        <td>38</td>
                        <td>20</td>
                        <td>18</td>
                        <td>15</td>
                        <td>11</td>
                    </tr>
                    <tr>
                        <td>3 цвета</td>
                        <td>54</td>
                        <td>28</td>
                        <td>24</td>
                        <td>18</td>
                        <td>15</td>
                    </tr>
                    <tr>
                        <td>4 цвета</td>
                        <td>68</td>
                        <td>35</td>
                        <td>30</td>
                        <td>24</td>
                        <td>20</td>
                    </tr>
                    <tr>
                        <td>-</td>
                        <td class="hightlight">1000</td>
                        <td class="hightlight">2000</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>1 цвет</td>
                        <td>7</td>
                        <td>6</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>2 цвета</td>
                        <td>9</td>
                        <td>8,5</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>3 цвета</td>
                        <td>12</td>
                        <td>11</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>4 цвета</td>
                        <td>17</td>
                        <td>16</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                </table>

                <p class="print-table-description">
                    Изготовление формы - 500 рублей за каждый цвет<br/>
                    Краска золото, серебро +20%<br/>
                    Формат печати А3 +20%<br/>
                </p>


            </div>

            <span class="print-type-title js-toggle-active icon-pechat">Визитные карточки на бумаге touch cover (тач кавер, дизайнерская бумага)</span>
            <div class="print-type-table">
                <table>
                    <tr>
                        <td>Тираж</td>
                        <td>1+0</td>
                        <td>1+1</td>
                        <td>2+0</td>
                        <td>2+1</td>
                        <td>2+2</td>
                    </tr>
                    <tr>
                        <td>100</td>
                        <td>16</td>
                        <td>23</td>
                        <td>23</td>
                        <td>32</td>
                        <td>45</td>
                    </tr>
                    <tr>
                        <td>200</td>
                        <td>11</td>
                        <td>17</td>
                        <td>17</td>
                        <td>23</td>
                        <td>28</td>
                    </tr>
                    <tr>
                        <td>500</td>
                        <td>8</td>
                        <td>12</td>
                        <td>12</td>
                        <td>16</td>
                        <td>18</td>
                    </tr>
                </table>
                <p>Краска золото или серебро +20% к заказу</p>

            </div>

            <span class="print-type-title js-toggle-active icon-znachki">Значки</span>
            <div class="print-type-table">
                <h2>Закатные</h2>
                <table>
                    <tr>
                        <td>Размер</td>
                        <td colspan="5">Количество</td>

                    </tr>
                    <tr>
                        <td>-</td>
                        <td>50 шт.</td>
                        <td>100 шт.</td>
                        <td>300 шт.</td>
                        <td>500 шт.</td>
                        <td>1000 шт.</td>
                    </tr>
                    <tr>
                        <td>d=38</td>
                        <td>18,00</td>
                        <td>16,50</td>
                        <td>16,00</td>
                        <td>14,00</td>
                        <td>11,00</td>
                    </tr>
                    <tr>
                        <td>d=56</td>
                        <td>28,00</td>
                        <td>24,50</td>
                        <td>24,00</td>
                        <td>22,00</td>
                        <td>14,00</td>
                    </tr>
                </table>

                <h2>Значки с заливкой смолой</h2>
                <table>
                    <tr>
                        <td>Форма</td>
                        <td colspan="5">Количество</td>

                    </tr>
                    <tr>
                        <td>-</td>
                        <td>50 шт</td>
                        <td>100 шт</td>
                        <td>300 шт</td>
                        <td>500 шт</td>
                    </tr>
                    <tr>
                        <td>Круг (d=13; 17; 21; 25; 35 мм)</td>
                        <td>100,00</td>
                        <td>87,00</td>
                        <td>77,00</td>
                        <td>67,00</td>
                    </tr>
                    <tr>
                        <td>Овал (15x28; 20x27; 20x12 мм)</td>
                        <td>100,00</td>
                        <td>87,00</td>
                        <td>77,00</td>
                        <td>67,00</td>
                    </tr>
                    <tr>
                        <td>Прямоугольник (12x17; 17x11; 33x13)</td>
                        <td>100,00</td>
                        <td>87,00</td>
                        <td>77,00</td>
                        <td>67,00</td>
                    </tr>
                    <tr>
                        <td>Квадрат (14x14; 17x17; 21x21; 25x25 мм)</td>
                        <td>100,00</td>
                        <td>87,00</td>
                        <td>77,00</td>
                        <td>67,00</td>
                    </tr>
                    <tr>
                        <td>Щит (14x17мм)</td>
                        <td>100,00</td>
                        <td>87,00</td>
                        <td>77,00</td>
                        <td>67,00</td>
                    </tr>
                </table>
            </div>

            <span class="print-type-title js-toggle-active icon-blocknoty">Тиснение</span>
            <div class="print-type-table">
                <table>
                    <tr>
                        <td>Тираж</td>
                        <td>Стоимость тиснения</td>
                    </tr>
                    <tr>
                        <td>10</td>
                        <td>136</td>
                    </tr>
                    <tr>
                        <td>20</td>
                        <td>86</td>
                    </tr>
                    <tr>
                        <td>30</td>
                        <td>67</td>
                    </tr>
                    <tr>
                        <td>50</td>
                        <td>50</td>
                    </tr>
                    <tr>
                        <td>100</td>
                        <td>35</td>
                    </tr>
                    <tr>
                        <td>200</td>
                        <td>24</td>
                    </tr>
                    <tr>
                        <td>500</td>
                        <td>18</td>
                    </tr>
                    <tr>
                        <td>1000</td>
                        <td>15</td>
                    </tr>
                </table>
                <p>Стоимость клише: 1 см<sup>2</sup> - 85 руб</p>

            </div>

            <span class="print-type-title js-toggle-active icon-umbrella">Гравировка</span>
            <div class="print-type-table">
                <h2>Лазерная гравировка по металлу</h2>
                <h4>Начальные расходы: 1000.00</h4>
                <table>
                    <tr>
                        <td>Тираж</td>
                        <td>до 6см<sup>2</sup></td>
                        <td>за каждый см<sup>2</sup> сверх 6</td>
                    </tr>
                    <tr>
                        <td>1-9</td>
                        <td>55</td>
                        <td>+0,6</td>
                    </tr>
                    <tr>
                        <td>10-19</td>
                        <td>50</td>
                        <td>+0,6</td>
                    </tr>
                    <tr>
                        <td>20-49</td>
                        <td>47</td>
                        <td>+0,6</td>
                    </tr>
                    <tr>
                        <td>50-99</td>
                        <td>38</td>
                        <td>+0,6</td>
                    </tr>
                    <tr>
                        <td>100-299</td>
                        <td>35</td>
                        <td>+0,6</td>
                    </tr>
                    <tr>
                        <td>300-499</td>
                        <td>31</td>
                        <td>+0,6</td>
                    </tr>
                    <tr>
                        <td>от 500</td>
                        <td>30</td>
                        <td>+0,6</td>
                    </tr>
                </table>

                <p class="print-table-description">Указана стоимость нанесения за единицу за одно место нанесения
                </p>

                <h2>Круговая гравировка</h2>
                <h4>Начальные расходы: 1500.00</h4>
                <table>
                    <tr>
                        <td>Тираж</td>
                        <td>до 100см<sup>2</sup></td>
                        <td>за каждый см<sup>2</sup> сверх 100</td>
                    </tr>
                    <tr>
                        <td>1-9</td>
                        <td>370</td>
                        <td>+1</td>
                    </tr>
                    <tr>
                        <td>10-19</td>
                        <td>296</td>
                        <td>+1</td>
                    </tr>
                    <tr>
                        <td>20-49</td>
                        <td>237</td>
                        <td>+1</td>
                    </tr>
                    <tr>
                        <td>50-99</td>
                        <td>213</td>
                        <td>+1</td>
                    </tr>
                    <tr>
                        <td>100-299</td>
                        <td>170</td>
                        <td>+1</td>
                    </tr>
                    <tr>
                        <td>от 300</td>
                        <td>136</td>
                        <td>+1</td>
                    </tr>
                </table>

                <p class="print-table-description">Указана стоимость нанесения за единицу за одно место нанесения
                </p>
            </div>


        </div>
        <div class="text-block"><?php echo $description; ?></div>
    </div>
    <?php echo $content_bottom; ?>
</div>

<?php echo $footer; ?>