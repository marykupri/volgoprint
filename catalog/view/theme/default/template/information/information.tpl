<?php echo $header; ?>
  <div class="catalog_header catalog_header-category">
    <h1><?php echo $heading_title; ?></h1>
    <div class="breadcrumb">
      <?php $i=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php if($i<sizeof($breadcrumbs)-1){ ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php }else{ ?>
        <?php echo $breadcrumb['separator']; ?><?php echo $breadcrumb['text']; ?>
        <?php } ?>
      <?php $i++; } ?>
    </div>
  </div>

  <?php echo $column_left; ?><?php echo $column_right; ?>

  <div class="container container-information">
    <div id="content">
      <?php echo $content_top; ?>
      <?php echo $description; ?>
    </div>
  <?php echo $content_bottom; ?>
  </div>

<?php echo $footer; ?>