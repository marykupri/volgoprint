</div></div>

<div class="main-slider-block">
  <div class="main-slider-block-wrapper">

      <?php foreach ($banners as $banner) { ?>
        <div class="main-slider-slide">
          <div class="main-slider-text" id="bannerMessage">
            <strong class='main-slider-text-strong'><?php if($banner['link']!=""){ echo "<a href='".$banner['link']."'>".$banner['title']."</a>"; }else{ echo $banner['title']; } ?></strong>
            <span class="main-slider-text-description"><?php if($banner['link']!=""){ echo "<a href='".$banner['link']."'>".$banner['description']."</a>"; }else{ echo $banner['description']; } ?></span>
          </div>
          <div class="main-slider-image"  id="bannerImage"><img src="<?php echo $banner['image']; ?>"/></div>
        </div>
      <?php } ?>
  </div>
  <div class="main-slider-navigation">
     <div class="main-slider-holder">
    <?php $i=0; foreach ($banners as $banner) { ?>
      <div class="main-slider-navigation-bullet" data-i="<?php echo $i;?>"><?php echo $banner['caption']; ?></div>
    <? $i++; } ?>
      </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
      $('.main-slider-slide').eq(0).addClass('active');
      $('.main-slider-navigation-bullet').eq(0).addClass('active');

      var currentItem=0;
      var totalItem=$('.main-slider-slide').length;

      $('.main-slider-navigation-bullet').click(function(){
        var num=parseInt($(this).attr('data-i'));
          $('.main-slider-navigation-bullet').removeClass('active');
          $(this).addClass('active');
    
          $('.main-slider-slide.active').addClass('say-bye').delay(500).removeClass('active');
          setTimeout(
            function() 
            {
                    $('.main-slider-slide').eq(num).addClass('active');
                    $('.main-slider-slide').removeClass('say-bye');
            }, 500);
       


      });


      setInterval(function(){
        if(currentItem+1>=totalItem){
          currentItem=0;
        }else{
          currentItem=currentItem+1;
        }

        $('.main-slider-navigation-bullet').removeClass('active');
        $('.main-slider-navigation-bullet').eq(currentItem).addClass('active');
    
        $('.main-slider-slide.active').addClass('say-bye').delay(500).removeClass('active');
          setTimeout(
            function() 
            {
                    $('.main-slider-slide').eq(currentItem).addClass('active');
                    $('.main-slider-slide').removeClass('say-bye');
            }, 500);
       


      },7000);
  });

</script>
