<script type="text/javascript">
    var feed = new Instafeed({
        get: 'user',
        userId: '1268766989',
    limit:90,
    accessToken: '1268766989.1677ed0.11fe9b83970149db858c4680c31ea7d2',
    resolution: 'standard_resolution',
        template: '<li><a href="{{link}}" target=_blank><img src="{{image}}" /></a></li>',
    after: function(){
      $('#instafeed').bxSlider({
        pager:false,
        minSlides:5,
        maxSlides:5,
        slideWidth:175,
        slideMargin:27,
        moveSlides:1,
        nextSelector:'#goRight',
        prevSelector:'#goLeft'
      });
    }

    });
    feed.run();
</script>

<div id="instagram" class="box box-clean box-block">
  <div class="box-heading">Фотопоток <a href="https://www.instagram.com/volgoprint/" target="_blank" class="module-link instagram-external">Мы в Instagram</a></div>
  <div id="instafeed" class="instafeed"></div>
  <div class="instagram-navigation instagram-navigation_goLeft" id="goLeft"></div>
  <div class="instagram-navigation instagram-navigation_goRight" id="goRight"></div>
</div>  




<?php if ($news) { ?>
<div class="box box-clean box-news-holder box-block">
  <div class="box-heading"><?php echo $heading_title; ?> <a href="/index.php?route=information/news" class="module-link module-link_news">Все новости</a></div>
  <div class="box-content">
    <?php foreach ($news as $news_story) { ?>
      <div class="box-news">
        <div class="box-image"><a href="<?php echo $news_story['href']; ?>"><? if($news_story['image']!=""){ echo "<img src='".$news_story['image']."' alt='".$news_story['title']."'/>"; }?></a></div>
        <div class="box-text"><a href="<?php echo $news_story['href']; ?>"><h4><?php echo $news_story['title']; ?></h4></a>
         <a href="<?php echo $news_story['href']; ?>"><?php echo $news_story['description']; ?></a>
        </div>
      </div>
    <?php } ?>
  </div>
</div>
<?php } ?>
