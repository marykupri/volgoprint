<?php echo $header; ?>

  <div class="catalog_header catalog_header-account">
    <h1><?php echo $heading_title; ?></h1>
    <div class="breadcrumb">
      <?php $i=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php if($i<sizeof($breadcrumbs)-1){ ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php }else{ ?>
        <?php echo $breadcrumb['separator']; ?><?php echo $breadcrumb['text']; ?>
        <?php } ?>
      <?php $i++; } ?>
    </div>
  </div>





  <div class="container container-account">
<div id="content"><?php echo $content_top; ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
  

  <h2><?php echo $text_my_account; ?></h2>
  <div class="content">
    <ul>
      <li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
      <li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
      <li><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></li>
      <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
      <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
    </ul>
  </div>

  <?php echo $content_bottom; ?></div></div>
<?php echo $footer; ?> 