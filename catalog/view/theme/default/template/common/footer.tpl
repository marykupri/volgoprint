</div>
<footer>
<div id="footer">

  <div class="column column-logo">
    <a href="/"><img src="/image/logo-white.png"/></a>
  </div>


  <?php if ($informations) { ?>
  <div class="column information-links">
     <ul>
      <?php foreach ($informations as $information) { ?>
      <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
      <?php } ?>
      <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
      <li><a href="/index.php?route=information/news">Портфолио</a></li>
    </ul>
  </div>
  <?php } ?>
 
  

   <div class="column column-address">
    г. Волгоград<br/>
    ул. Бакинская, 8
    <a href="<?php echo $contact; ?>">Как проехать</a>
  </div>

  <div class="column column-phone">
    <a href="tel:+78442982245">+7 (8442) 98-22-45</a>
    <a href="tel:+78442430535">+7 (8442) 43-05-35</a>
    <a href="mailto:profipechat@gmail.com">profipechat@gmail.com</a>
  </div>

  <div class="column column-social">
    <ul>
      <li><a href="https://www.youtube.com/channel/UClP8DocsTK2QzmnUSj2lotQ/videos" target="_blank" class="social social-tube">Youtube</a></li>
      <li><a href="https://www.instagram.com/volgoprint/" target="_blank" class="social social-in">Instagram</a></li>
      <li><a href="http://vk.com/hello_volgoprint34"  target="_blank" class="social social-vk">ВКонтакте</a></li>
    </ul>
  </div>
 
  <div class="copyrights">
    <span>&copy; 2010 &ndash; 2016 год. Все права защищены</span>
    <span>Разработано студией <a href="http://monodigital.ru" target="_blank">MONO</a></span>
  </div> 

</div>
</footer>

        <!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter35375055 = new Ya.Metrika({ id:35375055, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/35375055" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->

</body></html>