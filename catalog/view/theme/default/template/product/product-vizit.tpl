<?php echo $header; ?>

  <div class="catalog_header catalog_header-product">
    <h1><?php echo $heading_title; ?></h1>
    <div class="breadcrumb">
      <?php $i=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php if($i<sizeof($breadcrumbs)-1){ ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php }else{ ?>
        <?php echo $breadcrumb['separator']; ?><?php echo $breadcrumb['text']; ?>
        <?php } ?>
      <?php $i++; } ?>
  </div>
  </div>

    <div class="catalog_filters catalog_filters-product">
      <div class="container">
        <a href="<?php echo $breadcrumbs[sizeof($breadcrumbs)-2]['href'];?>" class="goToCatalog">&larr; Вернуться к каталогу</a>
      </div>
    </div>

  <div class="container container-information">
    <div id="content">
    <div class="print-type-holder">
  <?php echo $content_top; ?>


   

 <span class="print-type-title js-toggle-active icon-pechat">Визитные карточки</span>
<br/><br/>

<table>
    <tr>
        <td>Тираж</td>
        <td>1+0</td>
        <td>1+1</td>
        <td>2+0</td>
        <td>2+1</td>
        <td>2+2</td>
    </tr>    
    <tr>
        <td>100</td>
        <td>16</td>
        <td>23</td>
        <td>23</td>
        <td>32</td>
        <td>45</td>
    </tr>  
    <tr>
        <td>200</td>
        <td>11</td>
        <td>17</td>
        <td>17</td>
        <td>23</td>
        <td>28</td>
    </tr>  
    <tr>
        <td>500</td>
        <td>8</td>
        <td>12</td>
        <td>12</td>
        <td>16</td>
        <td>18</td>
    </tr>
</table>
<p>Краска золото или серебро +20% к заказу</p>


      <form class="calc-query-form">


    <div class="print-calculator-row top-separator">
      <input type="hidden" name="calcUserType" value="Визитные карточки"/>
        <label class="print-row-label-title">Имя:</label>
        <input type="text" class="input-box required" name="calcUserName" placeholder="Иванов Иван"/>
    </div>
    <div class="print-calculator-row top-separator">
        <label class="print-row-label-title">E-mail/Телефон:</label>
        <input type="text" class="input-box required" name="calcUserEmail" placeholder="hello@mail.ru"/>
    </div>

    <div class="print-calculator-row top-separator">
                <span class="calc-spans">Тираж:</span>
        <div class="select-box s-calculator-quantity js-exec-calc">
            <div class="select-box-current js-toggle-active" value="100">100</div>
             <div class="select-box-option-holder">
            <div class="select-box-option js-set-select-box-option"  value='100'>100</div>
            <div class="select-box-option js-set-select-box-option"  value='200'>200</div>
            <div class="select-box-option js-set-select-box-option"  value='500'>500</div>
            </div>
        </div>
    </div>

    <div class="print-calculator-row top-separator">
                <span class="calc-spans">Печать:</span>
        <div class="select-box s-calculator-shape js-exec-calc">
            <div class="select-box-current js-toggle-active" value="1+0">1+0</div>
             <div class="select-box-option-holder">
            <div class="select-box-option js-set-select-box-option"  value='1+0'>1+0</div>
            <div class="select-box-option js-set-select-box-option"  value='1+1'>1+1</div>
            <div class="select-box-option js-set-select-box-option"  value='2+0'>2+0</div>
            <div class="select-box-option js-set-select-box-option"  value='2+1'>2+1</div>
            <div class="select-box-option js-set-select-box-option"  value='2+2'>2+2</div>
            </div>
        </div>
    </div>

      <div class="print-calculator-row top-separator">
        <input type="button" class="js-send-calc input-button" value="Отправить заявку"/>
    </div>

    <div class="print-calculator-row top-separator form-success">
        <h2>Спасибо, ваша заявка успешно отправлена</h2>
    </div>
    </form>



  <?php echo $content_bottom; ?></div></div>

<?php echo $footer; ?>