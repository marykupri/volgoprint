<?php echo $header; ?>

  <div class="catalog_header catalog_header-product">
    <h1><?php echo $heading_title; ?></h1>
    <div class="breadcrumb">
      <?php $i=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php if($i<sizeof($breadcrumbs)-1){ ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php }else{ ?>
        <?php echo $breadcrumb['separator']; ?><?php echo $breadcrumb['text']; ?>
        <?php } ?>
      <?php $i++; } ?>
  </div>
  </div>

    <div class="catalog_filters catalog_filters-product">
      <div class="container">
        <a href="<?php echo $breadcrumbs[sizeof($breadcrumbs)-2]['href'];?>" class="goToCatalog">&larr; Вернуться к каталогу</a>
      </div>
    </div>

  <div class="container container-information">
    <div id="content">
    <div class="print-type-holder">
  <?php echo $content_top; ?>


   

 <span class="print-type-title js-toggle-active icon-baloon">Печать на шарах</span>
<br/><br/>
<h2>12 дюймов (30 см)</h2>
<table>
    <tr>
        <td rowspan="2">Тираж</td>
        <td colspan="2">12" Пастель</td>
        <td colspan="2">12" Металлик</td>
    </tr>
    <tr>
        <td>1 цвет, 1 сторона</td>
        <td>1 цвет, 2 стороны</td>
        <td>1 цвета, 1 сторона</td>
        <td>1 цвет, 2 стороны</td>
    </tr>
    <tr>
        <td>100</td>
        <td>26,00</td>
        <td>55,00</td>
        <td>29,4</td>
        <td>55,7</td>
    </tr>
    <tr>
        <td>200</td>
        <td>18,07</td>
        <td>36,3</td>
        <td>20,06</td>
        <td>36,9</td>
    </tr>
    <tr>
        <td>300</td>
        <td>14,77</td>
        <td>29,4</td>
        <td>18,1</td>
        <td>36,2</td>
    </tr>
    <tr>
        <td>500</td>
        <td>10,77</td>
        <td>22,8</td>
        <td>15,36</td>
        <td>27,67</td>
    </tr>
    <tr>
        <td>1000</td>
        <td>8,37</td>
        <td>16,15</td>
        <td>12,06</td>
        <td>20,9</td>
    </tr>
    <tr>
        <td>2000</td>
        <td>8,22</td>
        <td>15,05</td>
        <td>11,2</td>
        <td>19,23</td>
    </tr>
    <tr>
        <td>3000</td>
        <td>7,777</td>
        <td>13,73</td>
        <td>10,08</td>
        <td>17,03</td>
    </tr>
    <tr>
        <td>5000</td>
        <td>6,80</td>
</table>
<br/><br/>
<h2>10 дюймов (23см)</h2>
<table>
    <tr>
        <td>Тираж</td>
        <td>Пастель</td>
        <td>Металлик</td>
    </tr>

    <tr>
        <td>-</td>
        <td>1 цвет</td>
        <td>1 цвет</td>
    </tr>
    <tr>
        <td>100</td>
        <td>25,00</td>
        <td>28,4</td>
    </tr>
    <tr>
        <td>200</td>
        <td>17,07</td>
        <td>19,06</td>
    </tr>
    <tr>
        <td>300</td>
        <td>13,77</td>
        <td>14,1</td>
    </tr>
    <tr>
        <td>500</td>
        <td>9,77</td>
        <td>11,36</td>
    </tr>
    <tr>
        <td>1000</td>
        <td>7,37</td>
        <td>8,06</td>
    </tr>
    <tr>
        <td>2000</td>
        <td>6,77</td>
        <td>7,2</td>
    </tr>
    <tr>
        <td>3000</td>
        <td>6,17</td>
        <td>6,08</td>
    </tr>
    <tr>
        <td>5000</td>
        <td>5,80</td>
        <td>6</td>
    </tr>

</table>

      <form class="calc-query-form">


    <div class="print-calculator-row top-separator">
      <input type="hidden" name="calcUserType" value="Шары"/>
        <label class="print-row-label-title">Имя:</label>
        <input type="text" class="input-box required" name="calcUserName" placeholder="Иванов Иван"/>
    </div>
    <div class="print-calculator-row top-separator">
        <label class="print-row-label-title">E-mail/Телефон:</label>
        <input type="text" class="input-box required" name="calcUserEmail" placeholder="hello@mail.ru"/>
    </div>

    <div class="print-calculator-row top-separator">
                <span class="calc-spans">Тираж:</span>
        <div class="select-box s-calculator-quantity js-exec-calc">
            <div class="select-box-current js-toggle-active" value="100">100</div>
             <div class="select-box-option-holder">
            <div class="select-box-option js-set-select-box-option"  value='100'>100</div>
            <div class="select-box-option js-set-select-box-option"  value='200'>200</div>
            <div class="select-box-option js-set-select-box-option"  value='300'>300</div>
            <div class="select-box-option js-set-select-box-option"  value='500'>500</div>
            <div class="select-box-option js-set-select-box-option"  value='1000'>1000</div>
            <div class="select-box-option js-set-select-box-option"  value='1500'>1500</div>
            <div class="select-box-option js-set-select-box-option"  value='2000'>2000</div>
            <div class="select-box-option js-set-select-box-option"  value='5000'>5000</div>
            </div>
        </div>
    </div>

    <div class="print-calculator-row top-separator">
                <span class="calc-spans">Размер:</span>
        <div class="select-box s-calculator-shape js-exec-calc">
            <div class="select-box-current js-toggle-active" value="12">12 дюймов</div>
             <div class="select-box-option-holder">
            <div class="select-box-option js-set-select-box-option"  value='12'>12 дюймов</div>
            <div class="select-box-option js-set-select-box-option"  value='10'>10 дюймов</div>
            </div>
        </div>
    </div>

      <div class="print-calculator-row top-separator">
        <input type="button" class="js-send-calc input-button" value="Отправить заявку"/>
    </div>

    <div class="print-calculator-row top-separator form-success">
        <h2>Спасибо, ваша заявка успешно отправлена</h2>
    </div>
    </form>



  <?php echo $content_bottom; ?></div></div>

<?php echo $footer; ?>