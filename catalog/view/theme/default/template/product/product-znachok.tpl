<?php echo $header; ?>

  <div class="catalog_header catalog_header-product">
    <h1><?php echo $heading_title; ?></h1>
    <div class="breadcrumb">
      <?php $i=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php if($i<sizeof($breadcrumbs)-1){ ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php }else{ ?>
        <?php echo $breadcrumb['separator']; ?><?php echo $breadcrumb['text']; ?>
        <?php } ?>
      <?php $i++; } ?>
  </div>
  </div>

    <div class="catalog_filters catalog_filters-product">
      <div class="container">
        <a href="<?php echo $breadcrumbs[sizeof($breadcrumbs)-2]['href'];?>" class="goToCatalog">&larr; Вернуться к каталогу</a>
      </div>
    </div>

  <div class="container container-information">
    <div id="content">
    <div class="print-type-holder">
  <?php echo $content_top; ?>


   

 <span class="print-type-title print-type-title icon-znachki">Значки</span>
<br/><br/>
<h2>Закатные</h2>
<table>
    <tr>
        <td>Размер</td>
        <td colspan="5">Количество</td>

    </tr>
    <tr>
        <td>-</td>
        <td>50 шт.</td>
        <td>100 шт.</td>
        <td>300 шт.</td>
        <td>500 шт.</td>
        <td>1000 шт.</td>
    </tr>
    <tr>
        <td>d=38</td>
        <td>18,00</td>
        <td>16,50</td>
        <td>16,00</td>
        <td>14,00</td>
        <td>11,00</td>
    </tr>
    <tr>
        <td>d=56</td>
        <td>28,00</td>
        <td>24,50</td>
        <td>24,00</td>
        <td>22,00</td>
        <td>14,00</td>
    </tr>
</table>

<h2>Значки с заливкой смолой</h2>
<table>
    <tr>
        <td>Форма</td>
        <td colspan="5">Количество</td>

    </tr>
    <tr>
        <td>-</td>
        <td>50 шт</td>
        <td>100 шт</td>
        <td>300 шт</td>
        <td>500 шт</td>
    </tr>
    <tr>
        <td>Круг (d=13; 17; 21; 25; 35 мм)</td>
        <td>100,00</td>
        <td>87,00</td>
        <td>77,00</td>
        <td>67,00</td>
    </tr>
    <tr>
        <td>Овал (15x28; 20x27; 20x12 мм)</td>
        <td>100,00</td>
        <td>87,00</td>
        <td>77,00</td>
        <td>67,00</td>
    </tr>
    <tr>
        <td>Прямоугольник (12x17; 17x11; 33x13)</td>
        <td>100,00</td>
        <td>87,00</td>
        <td>77,00</td>
        <td>67,00</td>
    </tr>
    <tr>
        <td>Квадрат (14x14; 17x17; 21x21; 25x25 мм)</td>
        <td>100,00</td>
        <td>87,00</td>
        <td>77,00</td>
        <td>67,00</td>
    </tr>
    <tr>
        <td>Щит (14x17мм)</td>
        <td>100,00</td>
        <td>87,00</td>
        <td>77,00</td>
        <td>67,00</td>
    </tr>
</table>

      <form class="calc-query-form">
      <input type="hidden" name="calcUserType" value="Значок"/>
    <div class="print-calculator-row top-separator">
        <label class="print-row-label-title">Имя:</label>
        <input type="text" class="input-box required" name="calcUserName" placeholder="Иванов Иван"/>
    </div>
    <div class="print-calculator-row top-separator">
        <label class="print-row-label-title">E-mail/Телефон:</label>
        <input type="text" class="input-box required" name="calcUserEmail" placeholder="hello@mail.ru"/>
    </div>
    <div class="print-calculator-row top-separator">
                <span class="calc-spans">Тираж:</span>
        <div class="select-box s-calculator-quantity js-exec-calc">
            <div class="select-box-current js-toggle-active" value="100">100</div>
             <div class="select-box-option-holder">
            <div class="select-box-option js-set-select-box-option"  value='100'>100</div>
            <div class="select-box-option js-set-select-box-option"  value='200'>200</div>
            <div class="select-box-option js-set-select-box-option"  value='300'>300</div>
            <div class="select-box-option js-set-select-box-option"  value='500'>500</div>
            <div class="select-box-option js-set-select-box-option"  value='1000'>1000</div>
            <div class="select-box-option js-set-select-box-option"  value='1500'>1500</div>
            <div class="select-box-option js-set-select-box-option"  value='2000'>2000</div>
            <div class="select-box-option js-set-select-box-option"  value='5000'>5000</div>
            </div>
        </div>
    </div>

<div class="print-calculator-row top-separator">
                <span class="calc-spans">Размер:</span>
        <div class="select-box s-calculator-shape js-exec-calc">
            <div class="select-box-current js-toggle-active" value="Круг">Круг (d=13; 17; 21; 25; 35 мм)</div>
             <div class="select-box-option-holder">
            <div class="select-box-option js-set-select-box-option"  value='d=38'>d=38</div>
            <div class="select-box-option js-set-select-box-option"  value='d=56'>d=56</div>
            <div class="select-box-option js-set-select-box-option"  value='Круг'>Круг (d=13; 17; 21; 25; 35 мм)</div>
            <div class="select-box-option js-set-select-box-option"  value='Овал'>Овал (15x28; 20x27; 20x12 мм)</div>
            <div class="select-box-option js-set-select-box-option"  value='Прямоугольник'>Прямоугольник (12x17; 17x11; 33x13)</div>
            <div class="select-box-option js-set-select-box-option"  value='Квадрат'>Квадрат (14x14; 17x17; 21x21; 25x25 мм)</div>
            <div class="select-box-option js-set-select-box-option"  value='Щит'>Щит (14x17мм)</div>
            </div>
        </div>
    </div>

    
      <div class="print-calculator-row top-separator">
        <input type="button" class="js-send-calc input-button" value="Отправить заявку"/>
    </div>

    <div class="print-calculator-row top-separator form-success">
        <h2>Спасибо, ваша заявка успешно отправлена</h2>
    </div>
    </form>

  <?php echo $content_bottom; ?></div></div>

<?php echo $footer; ?>