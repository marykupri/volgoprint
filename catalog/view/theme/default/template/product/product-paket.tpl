<?php echo $header; ?>

  <div class="catalog_header catalog_header-product">
    <h1><?php echo $heading_title; ?></h1>
    <div class="breadcrumb">
      <?php $i=0; foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php if($i<sizeof($breadcrumbs)-1){ ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php }else{ ?>
        <?php echo $breadcrumb['separator']; ?><?php echo $breadcrumb['text']; ?>
        <?php } ?>
      <?php $i++; } ?>
  </div>
  </div>

    <div class="catalog_filters catalog_filters-product">
      <div class="container">
        <a href="<?php echo $breadcrumbs[sizeof($breadcrumbs)-2]['href'];?>" class="goToCatalog">&larr; Вернуться к каталогу</a>
      </div>
    </div>

  <div class="container container-information">
    <div id="content">
    <div class="print-type-holder">
  <?php echo $content_top; ?>
  <?php echo $column_left; ?>
  <?php echo $column_right; ?>

   

 <span class="print-type-title icon-paket">Пакеты полиэтиленовые с вырубной ручкой</span>
   <div class="print-calculator">
    
    <div class="print-calculator-row">
        <label class="print-row-label-title">Размер пакета:</label>
        <label>
        <input class="size-radio js-exec-calc" type="radio" name="bag-size" value="4050" checked/>
        <span class="size-label" value="4050">40x50см</span>
        </label>
        <label>
        <input class="size-radio js-exec-calc" type="radio" name="bag-size" value="3040"/>
        <span class="size-label" value="3040">30x40см</span>
        </label>
        <label>
        <input class="size-radio js-exec-calc" type="radio" name="bag-size" value="2030"/>
        <span class="size-label" value="2030" >20x30см</span>
        </label>
        <label>
        <input class="size-radio js-exec-calc" type="radio" name="bag-size" value="9999"/>
        <span class="size-label">50х60 и 60х50</span>
        </label>
    </div>

    <div class="print-calculator-row pcr-2">

        <div class="col-3">
        <span class="calc-spans">Тираж:</span>
        <div class="select-box s-calculator-quantity js-exec-calc">
            <div class="select-box-current js-toggle-active" value="100">100</div>
             <div class="select-box-option-holder">
            <div class="select-box-option js-set-select-box-option"  value='100'>100</div>
            <div class="select-box-option js-set-select-box-option"  value='200'>200</div>
            <div class="select-box-option js-set-select-box-option"  value='300'>300</div>
            <div class="select-box-option js-set-select-box-option"  value='500'>500</div>
            <div class="select-box-option js-set-select-box-option"  value='1000'>1000</div>
            <div class="select-box-option js-set-select-box-option"  value='1500'>1500</div>
            <div class="select-box-option js-set-select-box-option"  value='2000'>2000</div>
            <div class="select-box-option js-set-select-box-option"  value='3000'>3000</div>
            <div class="select-box-option js-set-select-box-option"  value='5000'>5000</div>
            </div>
        </div>
        </div>
        <div class="col-3">
        <span class="calc-spans">Цвет:</span>
        <div class="select-box s-calculator-color js-exec-calc">
            <div class="select-box-current js-toggle-active" value="white">Белый</div>
             <div class="select-box-option-holder">
                <div class="select-box-option js-set-select-box-option"  data-color="#FFFFFF" value='FFFFFF'>Белый</div>
                <div class="select-box-option js-set-select-box-option"  data-color="#337b7a" value='337b7a'>Бирюзовый</div>
                <div class="select-box-option js-set-select-box-option"  data-color="#CC0000" value='CC0000'>Бордовый</div>
                <div class="select-box-option js-set-select-box-option"  data-color="#00CCFF" value='00CCFF'>Голубой мет.</div>
                <div class="select-box-option js-set-select-box-option"  data-color="#4b9dcd" value='4b9dcd'>Голубой</div>
                <div class="select-box-option js-set-select-box-option"  data-color="#fff71a" value='fff71a'>Жёлтый</div>
                <div class="select-box-option js-set-select-box-option"  data-color="#2c8c4c" value='2c8c4c'>Зеленый</div>
                <div class="select-box-option js-set-select-box-option"  data-color="#ce9e49" value='ce9e49'>Золото</div>
                <div class="select-box-option js-set-select-box-option"  data-color="#6b3d2d" value='6b3d2d'>Коричневый</div>
                <div class="select-box-option js-set-select-box-option"  data-color="#EF0504" value='EF0504'>Красный</div>
                <div class="select-box-option js-set-select-box-option"  data-color="#FF6600" value='FF6600'>Оранжевый</div>
                <div class="select-box-option js-set-select-box-option"  data-color="#f496b0" value='f496b0'>Розовый</div>
                <div class="select-box-option js-set-select-box-option"  data-color="#62af3f" value='62af3f'>Салатовый</div>
                <div class="select-box-option js-set-select-box-option"  data-color="#FFCC33" value='FFCC33'>Светло-беж.</div>
                <div class="select-box-option js-set-select-box-option"  data-color="#D8D8D8" value='D8D8D8'>Серебро</div>
                <div class="select-box-option js-set-select-box-option"  data-color="#0000FF" value='0000FF'>Синий</div>
                <div class="select-box-option js-set-select-box-option"  data-color="#009900" value='009900'>Тёмно-зеленый</div>
                <div class="select-box-option js-set-select-box-option"  data-color="#0596AF" value='0596AF'>Тёмно-синий</div>
                <div class="select-box-option js-set-select-box-option"  data-color="#8B00FF" value='8B00FF'>Фиолетовый</div>
                <div class="select-box-option js-set-select-box-option"  data-color="#000000" value='000000'>Черный</div>
            </div>
        </div>
        </div>
        <div class="col-3">
        <span class="calc-spans">Плотность пакета:</span>
        <div class="select-box s-calculator-thick js-exec-calc">
            <div class="select-box-current js-toggle-active" value="50">50 мкм</div>
            <div class="select-box-option-holder">
            <div class="select-box-option js-set-select-box-option"  value='50'>50 мкм</div>
            <div class="select-box-option js-set-select-box-option"  value='70'>70 мкм</div>
            <div class="select-box-option js-set-select-box-option"  value='80'>80 мкм</div>
            <div class="select-box-option js-set-select-box-option"  value='100'>100 мкм</div>
            </div>
        </div>
        </div>
        <div class="col-3">
        <span class="calc-spans">Кол-во цветов:</span>
        <div class="select-box s-calculator-colors js-exec-calc">
            <div class="select-box-current js-toggle-active" value="1">1</div>
             <div class="select-box-option-holder">
            <div class="select-box-option js-set-select-box-option"  value='1'>1</div>
            <div class="select-box-option js-set-select-box-option"  value='2'>2</div>
            <div class="select-box-option js-set-select-box-option"  value='3'>3</div>
            <div class="select-box-option js-set-select-box-option"  value='4'>4</div>
            <div class="select-box-option js-set-select-box-option"  value='5'>5</div>
            <div class="select-box-option js-set-select-box-option"  value='6'>6</div>
            <div class="select-box-option js-set-select-box-option"  value='7'>7</div>
            <div class="select-box-option js-set-select-box-option"  value='8'>8</div>
            <div class="select-box-option js-set-select-box-option"  value='9'>9</div>
            </div>
        </div>
        </div>
    </div>

    <div class="print-calculator-row">
        <input class="js-exec-calc" type="checkbox" name="isGold"><span>Краска золото, серебро</span>
        <span class="align-right">штука: <b class="s-cost-per-item">0 руб</b></span>
        <span class="align-right">тираж: <b class="s-cost-total">0 руб</b></span>
    </div>
      <form class="calc-query-form">
      <input type="hidden" name="calcUserType" value="Пакет"/>
    <div class="print-calculator-row top-separator">
        <label class="print-row-label-title">Имя:</label>
        <input type="text" class="input-box required" name="calcUserName" placeholder="Иванов Иван"/>
    </div>
    <div class="print-calculator-row top-separator">
        <label class="print-row-label-title">E-mail/Телефон:</label>
        <input type="text" class="input-box required" name="calcUserEmail" placeholder="hello@mail.ru"/>
    </div>
      <div class="print-calculator-row top-separator">
        <input type="button" class="js-send-calc input-button" value="Отправить заявку"/>
    </div>

    <div class="print-calculator-row top-separator form-success">
        <h2>Спасибо, ваша заявка успешно отправлена</h2>
    </div>
    </form>

</div>


<table id="dataBags" style="display:none">
    <tr>
        <td rowspan="2">Тираж, шт</td>
        <td colspan="3">Размер пакета 40х50</td>
        <td colspan="3">Размер пакета 30х40</td>
        <td colspan="3">Размер пакета 20х30</td>
    </tr>
    <tr>
        <td>1 цвет</td>
        <td>2 цвета</td>
        <td>3 цвета</td>
        <td>1 цвет</td>
        <td>2 цвета</td>
        <td>3 цвета</td>
        <td>1 цвет</td>
        <td>2 цвета</td>
        <td>3 цвета</td>
    </tr>
    <tr data-quantity="100">
        <td>100</td>
        <td>17.2</td>
        <td>23.3</td>
        <td>31.5</td>
        <td>16.5</td>
        <td>22.3</td>
        <td>30.2</td>
        <td>16.4</td>
        <td>22.2</td>
        <td>30.0</td>
    </tr>
    <tr data-quantity="200">
        <td>200</td>
        <td>15.2</td>
        <td>20.6</td>
        <td>27.9</td>
        <td>14.5</td>
        <td>19.6</td>
        <td>27.5</td>
        <td>14.4</td>
        <td>19.5</td>
        <td>26.4</td>
    </tr>
    <tr data-quantity="300">
        <td>300</td>
        <td>14.5</td>
        <td>19.6</td>
        <td>26.5</td>
        <td>13.4</td>
        <td>18.1</td>
        <td>24.5</td>
        <td>13.3</td>
        <td>18.0</td>
        <td>24.3</td>
    </tr>
    <tr data-quantity="500">
        <td>500</td>
        <td>13.4</td>
        <td>18.1</td>
        <td>24.5</td>
        <td>11.5</td>
        <td>15.6</td>
        <td>21.1</td>
        <td>11.4</td>
        <td>15.4</td>
        <td>20.8</td>
    </tr>
    <tr data-quantity="1000">
        <td>1000</td>
        <td>10.4</td>
        <td>14.1</td>
        <td>19.1</td>
        <td>8.7</td>
        <td>11.8</td>
        <td>16.0</td>
        <td>8.6</td>
        <td>11.7</td>
        <td>15.8</td>
    </tr>
    <tr data-quantity="1500">
        <td>1500</td>
        <td>9.7</td>
        <td>13.1</td>
        <td>17.7</td>
        <td>8.1</td>
        <td>11.0</td>
        <td>14.9</td>
        <td>8.0</td>
        <td>10.8</td>
        <td>14.5</td>
    </tr>
    <tr data-quantity="2000">
        <td>2000</td>
        <td>9.0</td>
        <td>12.2</td>
        <td>16.5</td>
        <td>7.5</td>
        <td>10.2</td>
        <td>13.8</td>
        <td>7.4</td>
        <td>10.0</td>
        <td>13.5</td>
    </tr>
    <tr data-quantity="3000">
        <td>3000</td>
        <td>8.8</td>
        <td>11.9</td>
        <td>16.1</td>
        <td>7.3</td>
        <td>9.9</td>
        <td>13.4</td>
        <td>7.0</td>
        <td>9.5</td>
        <td>12.9</td>
    </tr>
    <tr data-quantity="5000">
        <td>5000</td>
        <td>8.6</td>
        <td>11.7</td>
        <td>15.8</td>
        <td>7.1</td>
        <td>9.6</td>
        <td>13.0</td>
        <td>6.8</td>
        <td>9.2</td>
        <td>12.5</td>
    </tr>
    </table>


  </div>



  <?php echo $content_bottom; ?></div></div>

<?php echo $footer; ?>