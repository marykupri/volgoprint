<?php
// HTTP
define('HTTP_SERVER', 'http://vlgprint.loc/');

// HTTPS
define('HTTPS_SERVER', 'http://vlgprint.loc/');

// DIR
define('DIR_APPLICATION', 'C:/OpenServer/domains/vlgprint.loc/catalog/');
define('DIR_SYSTEM', 'C:/OpenServer/domains/vlgprint.loc/system/');
define('DIR_DATABASE', 'C:/OpenServer/domains/vlgprint.loc/system/database/');
define('DIR_LANGUAGE', 'C:/OpenServer/domains/vlgprint.loc/catalog/language/');
define('DIR_TEMPLATE', 'C:/OpenServer/domains/vlgprint.loc/catalog/view/theme/');
define('DIR_CONFIG', 'C:/OpenServer/domains/vlgprint.loc/system/config/');
define('DIR_IMAGE', 'C:/OpenServer/domains/vlgprint.loc/image/');
define('DIR_CACHE', 'C:/OpenServer/domains/vlgprint.loc/system/cache/');
define('DIR_DOWNLOAD', 'C:/OpenServer/domains/vlgprint.loc/download/');
define('DIR_LOGS', 'C:/OpenServer/domains/vlgprint.loc/system/logs/');

// DB
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'vlgprint');
define('DB_PREFIX', 'oc_');
?>