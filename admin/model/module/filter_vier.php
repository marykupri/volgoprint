<?php
/**
 * FilterVier_v.1.0-free for Opencart 1.5.x___OcStore 1.5.x
 **/
class ModelModuleFilterVier extends Model {
    
    //генерация Опций
    public function getOptions() {
        $poles = "
            od.`option_id`
            ,od.`name` AS name_group
            ,ovd.`option_value_id`
            ,ovd.`name` AS name_option
        ";
        $where = null;
        $where .= " WHERE od.`language_id` = ".(int)$this->config->get('config_language_id')." ";
        $query_str = "SELECT ".$poles." FROM
            `".DB_PREFIX."option` o
            LEFT JOIN `".DB_PREFIX."option_description` od ON (o.`option_id` = od.`option_id`)
            LEFT JOIN `".DB_PREFIX."option_value_description` ovd ON (od.`option_id` = ovd.`option_id`)
            
            ".$where."
            
            GROUP BY ovd.`option_value_id`
            ORDER BY o.`sort_order`, ovd.`name` ASC
            ";
		$query = $this->db->query($query_str);
        return $query->rows;
    }
    
    //генерация Категорий
    public function getCategorys() {
        $poles = "
            c.`parent_id`
            ,c.`category_id`
            ,cd.`name`
            ,c.`sort_order`
            ,p2c.`product_id`
            ,COUNT(p2c.`product_id`) AS total_product
        ";
        $where = null;
        $group_by = null;
        $order_by = null;
        $query_str = "SELECT ".$poles." FROM
            `".DB_PREFIX."category` c
            LEFT JOIN `".DB_PREFIX."category_description` cd ON (c.`category_id` = cd.`category_id`)
            LEFT JOIN `".DB_PREFIX."category_to_store` c2s ON (c.`category_id` = c2s.`category_id`) ";
            
        $query_str .= " LEFT JOIN `".DB_PREFIX."product_to_category` p2c ON (c.`category_id` = p2c.`category_id`) ";
        
        $where_stat_prod = " WHERE c.`status` != 0 ";
        $where .= $where_stat_prod;
        $where .= " AND c2s.`store_id` = 0 ";
        $where .= " AND cd.`language_id` = ".(int)$this->config->get('config_language_id')." ";
        $where .= " AND c2s.`store_id` = ".(int)$this->config->get('config_store_id')." ";        
        
        $group_by .= " GROUP BY c.`category_id` ";
        $order_by .= " ORDER BY c.`parent_id`, c.`sort_order` ,cd.`name` ASC ";
           
        $query_str .= $where.$group_by.$order_by;
		$query = $this->db->query($query_str);
        return $query->rows;
    }
    
    //генерация Атрибутов
    public function getAttributes() {
        $poles = "
            a.`attribute_id`
            ,a.`attribute_group_id`
            ,ad.`name` AS name_attr
            ,agd.`name` AS name_group
        ";
        $where = null;
        $where .= " WHERE ad.`language_id` = ".(int)$this->config->get('config_language_id')." ";
        $where .= " AND agd.`language_id` = ".(int)$this->config->get('config_language_id')." ";
        $query_str = "SELECT ".$poles." FROM 
            `".DB_PREFIX."attribute` a
            LEFT JOIN `".DB_PREFIX."attribute_description` ad ON (a.`attribute_id` = ad.`attribute_id`)
            LEFT JOIN `".DB_PREFIX."attribute_group_description` agd ON (a.`attribute_group_id` = agd.`attribute_group_id`)
            
            ".$where."
            GROUP BY a.`attribute_id`
            ORDER BY ad.`name`
            ";
		$query = $this->db->query($query_str);
        return $query->rows;
    }
}
?>