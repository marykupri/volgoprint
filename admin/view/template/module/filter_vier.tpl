<?php echo $header; ?>
<!-- /**
 * FilterVier_v.1.0-free for Opencart 1.5.x___OcStore 1.5.x
 **/ -->
<div id="content">
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="box">
  <div class="heading">
    <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
    <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
  </div>
  <div class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
    <!-- categorys -->
    <?php if(!empty($view_categorys)) { ?>
        <div class="block-param category">
        <fieldset style="background-color: #ccc; color: #999;">
        <legend><?php echo $legend_category; ?></legend>
        <span style="color: red;"> Commerce </span>
        <div class="status">
            <?php $chec = null; if(isset($filter_vier_setting['category']['status'])) { $chec = ' checked="checked"';} ?>
            <input type="checkbox" name="filter_vier_setting[category][status]" value="1" <?php echo $chec; ?> /> 
            <?php echo $text_status; ?>
            <span class="gran"></span>
            <?php $count = null; if(isset($filter_vier_setting['category']['count'])) { $count = ' checked="checked"';} ?>
            <input type="checkbox" name="filter_vier_setting[category][count]" value="1" <?php echo $count; ?> />
            <?php echo $text_count; ?>
            <span  class="sort_order">
                <?php echo $text_sort; ?>
                <?php $sort = null; if(isset($filter_vier_setting['category']['sort'])) { $sort = ' checked="checked"';} ?>
                <input type="checkbox" name="filter_vier_setting[category][sort]" value="1" <?php echo $sort; ?> />
            </span>
        </div>
        <ul class="block-category">
        <?php foreach($view_categorys as $key => $category) {  ?>
            <li class="group_attr">
            <?php $chec = null; 
                if(isset($filter_vier_setting['category']['view']) && in_array($category['category_id'], $filter_vier_setting['category']['view'])) { $chec = ' checked="checked"';} ?>
                <input type="checkbox" name="filter_vier_setting[category][view][<?php echo $category['category_id']; ?>]" value="<?php echo $category['category_id']; ?>" <?php echo $chec; ?> />
            <?php echo $category['name']; ?> <span class="count">(<?php echo $category['total_product']; ?>)</span>
                <input class="sort_order" name="filter_vier_setting[category][sort_order][<?php echo $category['category_id']; ?>]" type="text" value="<?php echo isset($filter_vier_setting['category']['sort_order'][$category['category_id']]) ? $filter_vier_setting['category']['sort_order'][$category['category_id']] : null; ?>" />
            </li>
            <?php if(isset($category['children'])) { ?>
                <ul>
                    <?php foreach($category['children'] as $categor) { ?>
                        <li class="group_attr1">
                        <?php $chec = null; 
                            if(isset($filter_vier_setting['category']['view']) && in_array($categor['category_id'], $filter_vier_setting['category']['view'])) { $chec = ' checked="checked"';} ?>
                            <input type="checkbox" name="filter_vier_setting[category][view][<?php echo $categor['category_id']; ?>]" value="<?php echo $categor['category_id']; ?>" <?php echo $chec; ?> />
                        <?php echo $categor['name']; ?> <span class="count">(<?php echo $categor['total_product']; ?>)</span>
                            <input class="sort_order" name="filter_vier_setting[category][sort_order][<?php echo $categor['category_id']; ?>]" type="text" value="<?php echo isset($filter_vier_setting['category']['sort_order'][$categor['category_id']]) ? $filter_vier_setting['category']['sort_order'][$categor['category_id']] : null; ?>" />
                        </li>
                        <?php if(isset($categor['children'])) { ?>
                            <ul>
                                <?php foreach($categor['children'] as $catego) { ?>
                                    <li class="group_attr2">
                                    <?php $chec = null; 
                                    if(isset($filter_vier_setting['category']['view']) && in_array($catego['category_id'], $filter_vier_setting['category']['view'])) { $chec = ' checked="checked"';} ?>
                                        <input type="checkbox" name="filter_vier_setting[category][view][<?php echo $catego['category_id']; ?>]" value="<?php echo $catego['category_id']; ?>" <?php echo $chec; ?> />
                                    <?php echo $catego['name']; ?> <span class="count">(<?php echo $catego['total_product']; ?>)</span>
                                        <input class="sort_order" name="filter_vier_setting[category][sort_order][<?php echo $catego['category_id']; ?>]" type="text" value="<?php echo isset($filter_vier_setting['category']['sort_order'][$catego['category_id']]) ? $filter_vier_setting['category']['sort_order'][$catego['category_id']] : null; ?>" />
                                    </li>
                                    <?php if(isset($catego['children'])) { ?>
                                        <ul>
                                            <?php foreach($catego['children'] as $categ) { ?>
                                                <li class="group_attr3">
                                                <?php $chec = null; 
                                                    if(isset($filter_vier_setting['category']['view']) && in_array($categ['category_id'], $filter_vier_setting['category']['view'])) { $chec = ' checked="checked"';} ?>
                                                    <input type="checkbox" name="filter_vier_setting[category][view][<?php echo $categ['category_id']; ?>]" value="<?php echo $categ['category_id']; ?>" <?php echo $chec; ?> />
                                                <?php echo $categ['name']; ?> <span class="count">(<?php echo $categ['total_product']; ?>)</span>
                                                    <input class="sort_order" name="filter_vier_setting[category][sort_order][<?php echo $categ['category_id']; ?>]" type="text" value="<?php echo isset($filter_vier_setting['category']['sort_order'][$categ['category_id']]) ? $filter_vier_setting['category']['sort_order'][$categ['category_id']] : null; ?>" />
                                                </li>
                                                
                                            <?php } ?>
                                        </ul>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                        <?php } ?>
                    <?php } ?>
                </ul>
            <?php } ?>
        <?php }?>
        </ul>
        </fieldset>
        </div>
    <?php } ?>
    <!-- /categorys -->
    <!-- attributes <p></p>&nbsp;&nbsp;-->
    <?php if(!empty($view_attributes)) { ?>
        <div class="block-param">
        <fieldset>
        <legend><?php echo $legend_attr; ?></legend>
        <div class="status">
            <?php $chec = null; if(isset($filter_vier_setting['attr']['status'])) { $chec = ' checked="checked"';} ?>
            <input type="checkbox" name="filter_vier_setting[attr][status]" value="1" <?php echo $chec; ?> />
            <?php echo $text_status; ?>
            <span class="gran"></span>
            <?php $grupi = null; if(isset($filter_vier_setting['attr']['inout'])) { $grupi = ' checked="checked"';} ?>
            <input type="checkbox" name="filter_vier_setting[attr][inout]" value="1" <?php echo $grupi; ?> />
            <?php echo $text_inout; ?>
            <span class="gran"></span>
            <?php $count = null; if(isset($filter_vier_setting['attr']['count'])) { $count = ' checked="checked"';} ?>
            <input type="checkbox" name="filter_vier_setting[attr][count]" value="1" <?php echo $count; ?> />
            <?php echo $text_count; ?>
            <span class="gran"></span>
            <?php $group_param = null; if(isset($filter_vier_setting['attr']['group'])) { $group_param = ' checked="checked"';} ?>
            <span style="color: red;"> Commerce </span>
            <input type="checkbox" name="filter_vier_setting[attr][group]" value="1" <?php echo $group_param; ?> />
            <span style="color: red;"><?php echo $text_group; ?></span>
        </div>
        <?php foreach($view_attributes as $attributes) { ?>
            <?php if(isset($attributes['attribute_group_id'])) { ?>
            <p class="group_attr"><?php echo $attributes['name_group']; ?></p>
            <?php } ?>
            <?php foreach($attributes['attr'] as $attr) { ?>
            <?php $chec = null; 
                if(isset($filter_vier_setting['attr']['view']) && in_array($attr['attribute_id'], $filter_vier_setting['attr']['view'])) { $chec = ' checked="checked"';} ?>
                <p class="params"><input type="checkbox" name="filter_vier_setting[attr][view][<?php echo $attr['attribute_id']; ?>]" value="<?php echo $attr['attribute_id']; ?>" <?php echo $chec; ?> /> <?php echo $attr['name_attr']; ?></p>
            <?php } ?>
        <?php } ?>
        </fieldset>
        </div>
    <?php } ?>
    <!-- /attributes -->
    <!-- options -->
    <?php if(!empty($view_options)) { ?>
        <div class="block-param">
        <fieldset>
        <legend><?php echo $legend_option; ?></legend>
        <div class="status">
            <?php $chec = null; if(isset($filter_vier_setting['option']['status'])) { $chec = ' checked="checked"';} ?>
            <input type="checkbox" name="filter_vier_setting[option][status]" value="1" <?php echo $chec; ?> /> 
            <?php echo $text_status; ?>
            <span class="gran"></span>
            <?php $grupi = null; if(isset($filter_vier_setting['option']['inout'])) { $grupi = ' checked="checked"';} ?>
            <input type="checkbox" name="filter_vier_setting[option][inout]" value="1" <?php echo $grupi; ?> />
            <?php echo $text_inout; ?>
            <span class="gran"></span>
            <?php $count = null; if(isset($filter_vier_setting['option']['count'])) { $count = ' checked="checked"';} ?>
            <input type="checkbox" name="filter_vier_setting[option][count]" value="1" <?php echo $count; ?> />
            <?php echo $text_count; ?>
            <span  class="sort_order">
            <span style="color: red;"> Commerce </span>
                <span style="color: red;"><?php echo $text_button; ?></span>
                <?php $button = null; if(isset($filter_vier_setting['option']['button'])) { $button = ' checked="checked"';} ?>
                <input type="checkbox" name="filter_vier_setting[option][button]" value="1" <?php echo $button; ?> />
            </span>
        </div>
        <?php foreach($view_options as $options) { ?>
            <?php /*if(isset($options['option_id'])) {*/ ?>
            <?php $chec = null; 
                if(isset($filter_vier_setting['option']['view']) && in_array($options['option_id'], $filter_vier_setting['option']['view'])) { $chec = ' checked="checked"';} ?>
            <p class="group_attr">
                <input type="checkbox" name="filter_vier_setting[option][view][<?php echo $options['option_id']; ?>]" value="<?php echo $options['option_id']; ?>" <?php echo $chec; ?> /> <?php echo $options['name_group']; ?>
                
            <?php $chec_img = null; 
                if(isset($filter_vier_setting['option']['image']) && in_array($options['option_id'], $filter_vier_setting['option']['image'])) { $chec_img = ' checked="checked"';} ?>  
                <span class="check_img"><input type="checkbox" name="filter_vier_setting[option][image][<?php echo $options['option_id']; ?>]" value="<?php echo $options['option_id']; ?>" <?php echo $chec_img; ?> /><?php echo 'image';?> </span>
            </p>
            <?php /*}*/ /* else { ?>
                <?php foreach($options['option'] as $option) { ?>
                <?php $chec = null; 
                    if(isset($filter_vier_setting['option']) && in_array($option['option_value_id'], $filter_vier_setting['option'])) { $chec = ' checked="checked"';} ?>
                    <p class="params">
                        <input type="checkbox" name="filter_vier_setting[option][<?php echo $option['option_value_id']; ?>]" value="<?php echo $option['option_value_id']; ?>" <?php echo $chec; ?> /> <?php echo $option['name_option']; ?>
                    </p>
                <?php } ?>
            <?php } */ ?>
        <?php } ?>
        </fieldset>
        </div>
    <?php } ?>
    <!-- /options -->
    <!-- price -->
        <div class="block-param">
        <fieldset>
        <legend><?php echo $legend_price; ?></legend> 
        <?php $chec = null; if(isset($filter_vier_setting['price']['status'])) { $chec = ' checked="checked"';} ?>
        <div class="status">
            <input type="checkbox" name="filter_vier_setting[price][status]" value="1" <?php echo $chec; ?> /> 
            <?php echo $text_status; ?>
            <span class="gran"></span>
            <?php $grupi = null; if(isset($filter_vier_setting['price']['inout'])) { $grupi = ' checked="checked"';} ?>
            <input type="checkbox" name="filter_vier_setting[price][inout]" value="1" <?php echo $grupi; ?> />
            <?php echo $text_inout; ?>
            <span class="gran"></span>
            <?php $count = null; if(isset($filter_vier_setting['price']['count'])) { $count = ' checked="checked"';} ?>
            <input type="checkbox" name="filter_vier_setting[price][count]" value="1" <?php echo $count; ?> />
            <?php echo $text_count; ?>
            <span class="gran"></span><span style="color: red;"> Commerce </span>
            <?php $slider = null; if(isset($filter_vier_setting['price']['slider'])) { $slider = ' checked="checked"';} ?>
            <input type="checkbox" name="filter_vier_setting[price][slider]" value="1" <?php echo $slider; ?> />
            <span style="color: red;"> slider </span>
            <?php //echo 'slider';//$text_count; ?>
        </div>
            <p class="group_attr">
                <?php echo $help_price_step; ?>
            </p>
            <p class="params">
            <p> <?php echo $help_price_separ; ?> - ;</p>
            <input class="input-price" type="text" name="filter_vier_setting[price][view]" value="<?php echo isset($filter_vier_setting['price']['view']) ? $filter_vier_setting['price']['view'] : null; ?>" /> <span><?php echo $this->config->get('config_currency'); ?></span>   
            </p>
        </fieldset>
        </div>
    <!-- /price -->
    <!-- manufacted -->
        <div class="block-param">
        <fieldset>
        <legend><?php echo $legend_manuf; ?></legend>
        <?php $chec = null; if(isset($filter_vier_setting['manuf']['status'])) { $chec = ' checked="checked"';} ?>
        <div class="status">
            <input type="checkbox" name="filter_vier_setting[manuf][status]" value="1" <?php echo $chec; ?> /> 
            <?php echo $text_status; ?>
            <span class="gran"></span>
            <?php $grupi = null; if(isset($filter_vier_setting['manuf']['inout'])) { $grupi = ' checked="checked"';} ?>
            <input type="checkbox" name="filter_vier_setting[manuf][inout]" value="1" <?php echo $grupi; ?> />
            <?php echo $text_inout; ?>
            <span class="gran"></span>
            <?php $count = null; if(isset($filter_vier_setting['manuf']['count'])) { $count = ' checked="checked"';} ?>
            <input type="checkbox" name="filter_vier_setting[manuf][count]" value="1" <?php echo $count; ?> />
            <?php echo $text_count; ?>
        </div>
        </fieldset>
        </div>
    <!-- /manufacted -->
    <!-- quantity -->
        <div class="block-param">
        <fieldset style="background-color: #ccc; color: #999;">
        <legend><?php echo $legend_quantity; ?></legend>
        <span style="color: red;"> Commerce </span>
        <?php $chec = null; if(isset($filter_vier_setting['quantity']['status'])) { $chec = ' checked="checked"';} ?>
        <div class="status">
            <input type="checkbox" name="filter_vier_setting[quantity][status]" value="1" <?php echo $chec; ?> /> 
            <?php echo $text_status; ?>
            <span class="gran"></span>
            <?php $count = null; if(isset($filter_vier_setting['quantity']['count'])) { $count = ' checked="checked"';} ?>
            <input type="checkbox" name="filter_vier_setting[quantity][count]" value="1" <?php echo $count; ?> />
            <?php echo $text_count; ?>
        </div>
        </fieldset>
        </div>
    <!-- /quantity -->
    <!-- nows -->
        <div class="block-param">
        <fieldset style="background-color: #ccc; color: #999;">
        <legend><?php echo $legend_news; ?></legend>
        <span style="color: red;"> Commerce </span>
        <?php $chec = null; if(isset($filter_vier_setting['nows']['status'])) { $chec = ' checked="checked"';} ?>
        <div class="status">
            <input type="checkbox" name="filter_vier_setting[nows][status]" value="1" <?php echo $chec; ?> /> 
            <?php echo $text_status; ?>
            <span class="gran"></span>
            <?php $count = null; if(isset($filter_vier_setting['nows']['count'])) { $count = ' checked="checked"';} ?>
            <input type="checkbox" name="filter_vier_setting[nows][count]" value="1" <?php echo $count; ?> />
            <?php echo $text_count; ?>
        </div>
        <p class="group_attr">
                <?php echo $help_news_text; ?>
            </p>
            <p class="params">
            <input class="input_right" type="text" name="filter_vier_setting[nows][day]" value="<?php echo isset($filter_vier_setting['nows']['day']) ? $filter_vier_setting['nows']['day'] : null; ?>" /> <span><?php echo $help_news_day; ?></span>
            </p>
        </fieldset>
        </div>
    <!-- /nows -->
    <div class="clear"></div>
      <table id="module" class="list">
        <thead>
          <tr>
            <td class="left"><?php echo $entry_layout; ?></td>
            <td class="left"><?php echo $entry_position; ?></td>
            <td class="left"><?php echo $entry_status; ?></td>
            <td class="right"><?php echo $entry_sort_order; ?></td>
            <td></td>
          </tr>
        </thead>
        <?php $module_row = 0; ?>
        <?php foreach ($modules as $module) { ?>
        <tbody id="module-row<?php echo $module_row; ?>">
          <tr>
            <td class="left"><select name="filter_vier_module[<?php echo $module_row; ?>][layout_id]">
                <?php foreach ($layouts as $layout) { ?>
                <?php if ($layout['layout_id'] == $module['layout_id']) { ?>
                <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select></td>
            <td class="left"><select name="filter_vier_module[<?php echo $module_row; ?>][position]">
                <?php if ($module['position'] == 'content_top') { ?>
                <option value="content_top" selected="selected"><?php echo $text_content_top; ?></option>
                <?php } else { ?>
                <option value="content_top"><?php echo $text_content_top; ?></option>
                <?php } ?>  
                <?php if ($module['position'] == 'content_bottom') { ?>
                <option value="content_bottom" selected="selected"><?php echo $text_content_bottom; ?></option>
                <?php } else { ?>
                <option value="content_bottom"><?php echo $text_content_bottom; ?></option>
                <?php } ?>     
                <?php if ($module['position'] == 'column_left') { ?>
                <option value="column_left" selected="selected"><?php echo $text_column_left; ?></option>
                <?php } else { ?>
                <option value="column_left"><?php echo $text_column_left; ?></option>
                <?php } ?>
                <?php if ($module['position'] == 'column_right') { ?>
                <option value="column_right" selected="selected"><?php echo $text_column_right; ?></option>
                <?php } else { ?>
                <option value="column_right"><?php echo $text_column_right; ?></option>
                <?php } ?>
              </select></td>
            <td class="left"><select name="filter_vier_module[<?php echo $module_row; ?>][status]">
                <?php if ($module['status']) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
            <td class="right"><input type="text" name="filter_vier_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $module['sort_order']; ?>" size="3" /></td>
            <td class="left"><a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
          </tr>
        </tbody>
        <?php $module_row++; ?>
        <?php } ?>
        <tfoot>
          <tr>
            <td colspan="4"></td>
            <td class="left"><a onclick="addModule();" class="button"><?php echo $button_add_module; ?></a></td>
          </tr>
        </tfoot>
      </table>
    </form>
  </div>
</div>
<script type="text/javascript"><!--
var module_row = <?php echo $module_row; ?>;

function addModule() {	
	html  = '<tbody id="module-row' + module_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><select name="filter_vier_module[' + module_row + '][layout_id]">';
	<?php foreach ($layouts as $layout) { ?>
	html += '      <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>';
	<?php } ?>
	html += '    </select></td>';
	html += '    <td class="left"><select name="filter_vier_module[' + module_row + '][position]">';
	html += '      <option value="content_top"><?php echo $text_content_top; ?></option>';
	html += '      <option value="content_bottom"><?php echo $text_content_bottom; ?></option>';
	html += '      <option value="column_left"><?php echo $text_column_left; ?></option>';
	html += '      <option value="column_right"><?php echo $text_column_right; ?></option>';
	html += '    </select></td>';
	html += '    <td class="left"><select name="filter_vier_module[' + module_row + '][status]">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';
	html += '    <td class="right"><input type="text" name="filter_vier_module[' + module_row + '][sort_order]" value="" size="3" /></td>';
	html += '    <td class="left"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#module tfoot').before(html);
	
	module_row++;
}
//--></script>
<?php echo $footer; ?>