<?php
/**
 * FilterVier_v.1.0-free for Opencart 1.5.x___OcStore 1.5.x
 **/
// Heading
$_['heading_title']     = '<b style="color:blue">FilterVier_v.1.0-free</b>';
$_['title']          	= 'FilterVier';

//Helps
$_['text_status'] = 'Статус';
$_['text_inout'] = 'InOut';
$_['text_count'] = 'Количество';
$_['text_group'] = 'Группы';
$_['text_sort'] = 'Сортировать';
$_['text_button'] = 'Кнопки';

$_['legend_category'] = 'Категории';
$_['legend_attr'] = 'Атрибуты';
$_['legend_option'] = 'Опции';
$_['legend_price'] = 'Цена';
$_['legend_manuf'] = 'Производители';
$_['legend_quantity'] = 'Наличие';
$_['legend_news'] = 'Новинки';

$_['help_news_day'] = 'Дней';
$_['help_news_text'] = 'За последние';
$_['help_price_separ'] = 'через точку с запятой';
$_['help_price_step'] = 'Шаг цены';


// Text
$_['text_module']         = 'Модуль';
$_['text_success']        = 'Настройки модуля сохранены!';
$_['text_content_top']    = 'Содержание шапки';
$_['text_content_bottom'] = 'Содержание подвала';
$_['text_column_left']    = 'Левая колонка';
$_['text_column_right']   = 'Правая колонка';

// Entry
$_['entry_layout']        = 'Схема:';
$_['entry_position']      = 'Расположение:';
$_['entry_status']        = 'Статус:';
$_['entry_sort_order']    = 'Порядок сортировки:';

// Error
$_['error_permission']    = 'Вы не имеете прав на управление данным модулем!';
?>