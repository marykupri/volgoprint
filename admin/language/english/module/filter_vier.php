<?php
/**
 * FilterVier_v.1.0-free for Opencart 1.5.x___OcStore 1.5.x
 **/
// Heading
$_['heading_title']   = '<b style="color:blue">FilterVier_v.1.0-free</b>';
$_['title']           = 'FilterVier';

//Helps
$_['text_status'] = 'Status';
$_['text_inout'] = 'InOut';
$_['text_count'] = 'Count';
$_['text_group'] = 'Groups';//View groupa
$_['text_sort'] = 'Sort';
$_['text_button'] = 'Buttos';

$_['legend_category'] = 'Category';
$_['legend_attr'] = 'Attributes';
$_['legend_option'] = 'Options';
$_['legend_price'] = 'Price';
$_['legend_manuf'] = 'Manufacturers';
$_['legend_quantity'] = 'Quantity';
$_['legend_news'] = 'News';

$_['help_news_day'] = 'Day';
$_['help_news_text'] = 'Latest';
$_['help_price_separ'] = 'separated by semicolons';
$_['help_price_step'] = 'Step price';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module category accordion!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';

// Entry
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module category accordion!';
?>