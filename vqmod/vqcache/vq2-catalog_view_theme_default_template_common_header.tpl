<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<meta name="description" content="<?php echo $description; ?>" />
<meta name="keywords" content="<?php echo $keywords; ?>" />
<link href="<?php echo $icon; ?>" rel="icon" />
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>

<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/jquery.bxslider.css" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/stylesheet.css" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/news.css" />
<?php foreach ($styles as $style) { ?>
<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
<script type="text/javascript" src="catalog/view/javascript/instagram.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/common.js"></script>
<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>


<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/3ceh.css" />


<?php echo $google_analytics; ?>
</head>
<body>
<header>
<div id="container">
<div id="header">
  
  <div id="logo">
    <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a>
    <span><a href="<?php echo $home; ?>">Рекламно-производственная компания</a></span>
  </div>
  
  <div id="site-description">Печать на футболках. Сувенирная и полиграфическая<br/> продукция в Волгограде.</div>
  
  <div id="top-address">
    <strong>г. Волгоград,</strong> <a href="/index.php?route=information/contact">ул. Бакинская, 8</a>
  </div>

  <div id="top-phone">
    <a href="tel:+78442982245">+7 (8442) 98-22-45</a>
    <a href="mailto:profipechat@gmail.com">profipechat@gmail.com</a>
  </div>

  <div id="search">
    <div class="button-search"></div>
    <input type="text" name="search" placeholder="<?php echo $text_search; ?>" value="<?php echo $search; ?>" />
  </div>

  <div id="welcome">
    <?php if (!$logged) { ?>
    <?php echo $text_welcome; ?>
    <?php } else { ?>
    <?php echo $text_logged; ?>
    <?php } ?>
  </div>
<?php if ($categories) { ?>
<div id="menuholder">
<div id="menu">
  <ul>
    <?php foreach ($categories as $category) { ?>
    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
      <?php if ($category['children']) { ?>
      <div>
        <?php for ($i = 0; $i < count($category['children']);) { ?>
        <ul>
          <?php $j = $i + ceil(count($category['children']) / $category['column']); ?>
          <?php for (; $i < $j; $i++) { ?>
          <?php if (isset($category['children'][$i])) { ?>
          <li class="menu-subcategory-item"><div class="menu-subcategory-svg"><a class="menu-subcategory-svg-link" href="<?php echo $category['children'][$i]['href']; ?>"><?php echo $category['children'][$i]['svg']; ?></a></div><a class="menu-subcategory-link" href="<?php echo $category['children'][$i]['href']; ?>"><?php echo $category['children'][$i]['name']; ?></a></li>
          <?php } ?>
          <?php } ?>
        </ul>
        <?php } ?>
      </div>
      <?php } ?>
    </li>
    <?php } ?>
      <li><a href="/print">Нанесение</a>
      <li><a href="/materials">Расходные материалы</a>
      <li><a href="/about">О компании</a>
      <li><a href="/index.php?route=information/news">Портфолио</a>
      <li><a href="/index.php?route=information/contact">Контакты</a>

  </ul>

  <?php echo $cart; ?>

</div>
</div>
<?php } ?>
</div>
</header>
<?php if ($error) { ?>
    
    <div class="warning"><?php echo $error ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
    
<?php } ?>
<div id="notification"></div>
