<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
<pre>
<?php
error_reporting(E_ALL);
set_time_limit(0);
/**
 * http://volgoprint.ru/_daemons/get_gifts_daemon.php
 * - сопоставление категорий на разных сайтах (выпадающий список - volgoprint.ru)
 * также устанавливает связи в таблице oc_product_to_category, но не удаляет их: надо внимательно пользоваться, либо модифицировать
 *
 * доп функции:
 * _daemons/get_gifts_daemon.php?step0=1
 * - начальные установки - парсит tree.xml - создаёт таблицы от gifts, и заполняет их
 *
 * _daemons/get_gifts_daemon.php?do_parse_products=1
 * - парсит stock.xml и product.xml и заполняет из них данные по товарам, скачивает изображения товаров
 * - xml-файлы - хранятся локально
 */

require_once "classes/database_class.php";

/**
 * @param string $url
 * @param string $params
 * @return bool|mixed
 */
function getResponse($url="", $params="") {
    //todo - настроить retry-delay

    $result = false;

    if (in_array('curl', get_loaded_extensions())) {
        $request = curl_init($url);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($request, CURLOPT_BINARYTRANSFER, 1);
        curl_setopt($request, CURLOPT_POST, true);
        curl_setopt($request, CURLOPT_POSTFIELDS, $params);
        $result = curl_exec($request);
    }

    if (!$result) {
        return false;
    }

    return $result;
}

/*
* Обновление временного статуса продукта на "выключен"
*/

function updateTmpStatusProducts(){
    global $db;

    $sql = "UPDATE `oc_product` SET `status_tmp`=0 where `shop`=1";

    $db->query($sql);
}

/*
* Обновление статуса продукта на актуальный временный статус
*/
function updateActiveStatusProduct(){
    global $db;

    $sql = "UPDATE `oc_product` SET `status`=`status_tmp` where `shop`=1";

    $db->query($sql);
}

/**
 * Рекурсивный пробег по tree.xml
 * @param $obj
 */
function xmlRecurseParceTree($obj) {
    global $db;

//    $page_arr = array();
//    $product_page_arr = array();

    if (isset($obj->page)) {

        foreach ($obj->page as $page) {

            $sql = "REPLACE INTO gifts_page SET
                page_id = '". (int)$page->page_id . "',
                name = '". $db->sql((string)$page->name) . "',
                uri = '". $db->sql((string)$page->uri) . "',
                parent_page_id = '". (isset($page['parent_page_id']) ? (int)$page['parent_page_id'] : 0) . "'";

            $db->query($sql);

            if (isset($page->product)) {
                foreach ($page->product as $product) {

                    if (intval($product->page) > 0 && intval($product->product) > 0) {
                        $sql = "REPLACE INTO gifts_product_to_page SET
                    page = '" . (int)$product->page . "',
                    product = '" . (int)$product->product . "'";

                        $db->query($sql);
                    }
                }
            }

            xmlRecurseParceTree($page);
        }
    }
}

/**
 * Парсинг filters.xml, заполнение таблиц фильтров
 */
function xmlParceFilters() {
    global $db;

    $obj = new SimpleXMLElement('gifts_files/filters.xml', NULL, TRUE);

    if (isset($obj->filtertypes) && isset($obj->filtertypes->filtertype)) {
        foreach ($obj->filtertypes->filtertype as $filtertype) {

            $sql = "SELECT * FROM gifts_filtertype WHERE `id` = '" . (int)$filtertype->filtertypeid . "'";
            $res = $db->query($sql);
            $res = $res->fetch_array();

            if (empty($res)) {
                $sql = "INSERT INTO oc_attribute SET
                        `attribute_group_id` = '1',
                        `sort_order` = '100'";
                $db->query($sql);
                $attribute_id = $db->insert_id;

                $sql = "INSERT INTO oc_attribute_description SET
                        `attribute_id` = '" . (int)$attribute_id . "',
                        `language_id` = '2',
                        `name` = '" . $db->sql((string)$filtertype->filtertypename) . "'";
                $db->query($sql);

                $sql = "INSERT INTO gifts_filtertype SET
                        `id` = '" . (int)$filtertype->filtertypeid . "',
                        `name` = '" . $db->sql((string)$filtertype->filtertypename) . "',
                        `attribute_id` = '" . (int)$attribute_id . "'";
                $db->query($sql);

            } elseif ($res['name'] != (string)$filtertype->filtertypename) {

                $sql = "UPDATE gifts_filtertype SET
                        `name` = '" . $db->sql((string)$filtertype->filtertypename) . "'
                        WHERE `id` = '" . (int)$filtertype->filtertypeid . "'";
                $db->query($sql);

                $sql = "UPDATE oc_attribute_description SET
                        `name` = '" . $db->sql((string)$filtertype->filtertypename) . "'
                        WHERE `attribute_id` = '" . (int)$res['attribute_id'] . "'
                        AND `language_id` = '2'";
                $db->query($sql);
            }

            if (isset($filtertype->filters) && isset($filtertype->filters->filter)) {

                foreach ($filtertype->filters->filter as $filter) {

                    $sql = "SELECT f.*, ft.attribute_id FROM gifts_filter as f
                            JOIN gifts_filtertype as ft
                            ON ft.id = f.type_id
                            WHERE f.`id` = '" . (int)$filter->filterid . "'
                            AND f.`type_id` = '" . (int)$filtertype->filtertypeid . "'";

                    $res = $db->query($sql);
                    $res = $res->fetch_array();

                    if (empty($res)) {
                        $sql = "INSERT INTO gifts_filter SET
                        `id` = '" . (int)$filter->filterid . "',
                        `type_id` = '" . (int)$filtertype->filtertypeid . "',
                        `name` = '" . $db->sql((string)$filter->filtername) . "'";
                        $db->query($sql);

                    } elseif ($res['name'] != (string)$filter->filtername) {

                        $sql = "UPDATE gifts_filter SET
                        `name` = '" . $db->sql((string)$filter->filtername) . "'
                        WHERE `id` = '" . (int)$filter->filterid . "'
                        AND `type_id` = '" . (int)$filtertype->filtertypeid . "'";
                        $db->query($sql);

                        $sql = "UPDATE oc_product_attribute as pa
                            JOIN gifts_filter_to_product as fp
                            ON fp.product_id = pa.product_id
                            AND fp.attribute_id = pa.attribute_id
                            JOIN gifts_filter as f
                            ON f.id = fp.filter_id
                            AND f.type_id = fp.filtertype_id
                        SET
                            `text` = '" . $db->sql((string)$filter->filtername) . "'
                        WHERE pa.`attribute_id` = '" . (int)$res['attribute_id'] . "'
                        AND pa.`language_id` = '2'
                        AND pa.`text` = '" . $db->sql($res['name']) . "'";

                        $db->query($sql);
                    }

                }
            }
        }
    }
}

/**
 * Парсинг stock.xml - получение информации о наличии товара на складе
 * @return array
 */
function xmlParceStock() {
    $obj = new SimpleXMLElement('gifts_files/stock.xml', NULL, TRUE);

    $data = array();

    foreach ($obj->stock as $stock) {
        $data[(int)$stock->product_id] = array(
            'product_id' => (int)$stock->product_id,
            'free' => (int)$stock->free,
            'amount' => (int)$stock->amount,
            'code' => (string)$stock->code,
            'enduserprice' => (string)$stock->enduserprice,
        );
    }

    return $data;
}

/**
 * Парсинг product.xml - получение описания товаров, которые есть на складе
 * @param $stock
 */
function xmlParceProduct($stock) {
    $obj = new SimpleXMLElement('gifts_files/product.xml', NULL, TRUE);

    $product_data = array();

    $loop = 15;

    foreach ($obj->product as $product) {

        $product_arr = array(
            'product_id' => (int)$product->product_id,
            'code' => (string)$product->code,
            'group' => (int)$product->group,
            'name' => (string)$product->name,
            'content' => (string)$product->content,
            'small_image' => (string)$product->small_image['src'],
            'big_image' => (string)$product->big_image['src'],
            'super_big_image' => (string)$product->super_big_image['src'],
            'product_size' => (string)$product->product_size,
            'price' => (string)$product->price->price,
            'currency' => (string)$product->price->currency,
            'weight' => (string)$product->weight,
            'matherial' => (string)$product->matherial,
            'quantity' => isset($stock[(int)$product->product_id]) ? (int)$stock[(int)$product->product_id]['free'] : 0,
            'status' => (int)$product->status
        );

        $img = false;
        if (isset($product->super_big_image['src'])) {
            $img = saveImg((string)$product->super_big_image['src']);
        }
        if (!$img && isset($product->big_image['src'])) {
            $img = saveImg((string)$product->big_image['src']);
        }
        if (!$img && isset($product->small_image['src'])) {
            $img = saveImg((string)$product->small_image['src']);
        }

        $product_arr['image'] = $img;

        $pr_arr = array();
        foreach($product->product as $pr){
            $pr_arr[] = array(
                'product_id' => (int)$pr->product_id,
                'code' => (string)$product->code,
                'size_code' => (string)$pr->size_code,
                'name' => (string)$pr->name,
                'price' => (string)$pr->price->price,
                'currency' => (string)$pr->price->currency,
                'quantity' => isset($stock[(int)$pr->product_id]) ? (int)$stock[(int)$product->product_id]['free'] : 0,
            );
        }
        if ($pr_arr) {
            $product_arr['product'] = $pr_arr;
        }


        $pr_img_arr = array();
        $pr_file_arr = array();
        $pr_img_local_arr = array();
        foreach($product->product_attachment as $pr){
            if ($pr->meaning) {
                $pr_img_arr[] = (string)$pr->image;
                if ($img = saveImg((string)$pr->image)) {
                    $pr_img_local_arr[] = $img;
                }
            } else {
                $pr_file_arr[] = (string)$pr->file;
            }
        }

        if ($pr_img_local_arr) {
            $product_arr['product_image'] = $pr_img_arr;
            $product_arr['product_image_local'] = $pr_img_local_arr;
        }
        if ($pr_file_arr) {
            $product_arr['product_file'] = $pr_file_arr;
        }

        $pr_filter_arr = array();
        if (isset($product->filters) && isset($product->filters->filter)){
            foreach($product->filters->filter as $filter){
                $pr_filter_arr[] = array(
                    'type' => $filter->filtertypeid,
                    'id' => $filter->filterid,
                );
            }
            $product_arr['filter'] = $pr_filter_arr;
        }

        $product_data[] = $product_arr;

        insertProduct($product_arr);

        $loop--;

////         для теста
//        if ($loop <= 0) {
//            break;
//        }
    }

//    print_r($product_data);
//    exit;
}

/////////////////////////////////////////////////
/**
 * Получение массива размеров
 * @return array
 */
function getSizes() {
    global $db;

    $sql = "select vd.option_value_id, vd.name
        from " . DB_PREFIX . "option_value_description as vd
        WHERE language_id = 2
        AND option_id = 13";
    $result_db = $db->query($sql);

    $data = array();

    while ($item = $result_db->fetch_array()) {
        $data[$item['option_value_id']] = $item['name'];
    }

    return $data;
}

function insertProduct($pr_data) {
    global $db;

    $additionalText="";
    if($pr_data['product_size']!=""){
       $additionalText="<b>Размер:</b> ".$pr_data['product_size']."<br/>";
    }
    $pr_data['content']=$additionalText."<b>Материал:</b> ".$pr_data['matherial']."<br/><b>Вес:</b> ".$pr_data['weight']."<br/><br/>".$pr_data['content'];
    $data = array(
        'model' => $pr_data['code'],
        'sku' => $pr_data['product_id'],
        'image' => $pr_data['image'],
        'price' => getPriceWithPercent($pr_data['price'], $pr_data['product_id']),
        //'price' => $pr_data['price'],
        'product_description' => array(2=>array(
            'name' => $pr_data['name'],
            'description' => $pr_data['content'],
        )),
        'stock_status_id' => $pr_data['quantity'] > 0 ? 6 : 5, // "Под заказ" или "Нет в наличии"
        'subtract' => 1,
        'status' => ($pr_data['status'] != 3 && $pr_data['image']) ? 1 : 0,
        'status_tmp' => 1,
        'shop' => 1,
        'minimum' => 1,
        'sort_order' => 1,
        'length_class_id' => 1,
        'weight_class_id' => 1,
        'shipping' => 1,
        'category' => getGiftsCategory($pr_data['product_id']),
        'quantity' => $pr_data['quantity'],
        'product_image' => isset($pr_data['product_image_local']) ? $pr_data['product_image_local'] : array(),
    );

    if (!$product_id = checkProductExist($pr_data['product_id'])) {
        $product_id = addProduct($data);

        // сохранение размера
        // сохраняем размер только для товара из категории "футболки и одежда"
        if(checkProductIsClothing($product_id)){
            $sql = "INSERT INTO `oc_product_option` (`product_id`, `option_id`, `required`) VALUES ('" . (int)$product_id . "', " . (int)$pr_data['filter_id'] . ", 1)";
            $db->query($sql);

            $product_option_id = $db->insert_id;

            if (isset($pr_data['product'])) {
                foreach ($pr_data['product'] as $pr) {
                    $size_code = $pr['size_code'];
                    $option_value_id = getOptionValueId($size_code);
                    addProductOption($product_id, $option_value_id, $product_option_id, $pr);

                }
            } else {
                $size_code = $pr_data['product_size'];
                $option_value_id = getOptionValueId($size_code);
                addProductOption($product_id, $option_value_id, $product_option_id, $pr_data);
            }
        }

    } else {
        $product_id = checkProductExist($pr_data['product_id']);
        updateProduct($data,$product_id);
    }

    // установка атрибутов
    if (!empty($pr_data['filter'])) {
        foreach ($pr_data['filter'] as $f) {

            $sql = "SELECT f.*, ft.attribute_id
                FROM gifts_filter as f
                JOIN gifts_filtertype as ft
                ON ft.id = f.type_id
                WHERE f.id = '" . (int)$f['id'] . "'
                AND f.type_id = '" . (int)$f['type'] . "'
                LIMIT 1";
            $res = $db->query($sql);
            $res = $res->fetch_array();

            if (!empty($res['attribute_id'])) {
                $sql = "REPLACE INTO `oc_product_attribute` (`product_id`, `attribute_id`, `language_id`, `text`)
                                VALUES ('" . (int)$product_id . "', '" . (int)$res['attribute_id'] . "', 2, '" . $db->sql($res['name']) . "')";
                $db->query($sql);


                $sql = "REPLACE INTO `gifts_filter_to_product` (`filter_id`, `filtertype_id`, `product_id`, `attribute_id`)
                                VALUES ('" . (int)$res['id'] . "', '" . (int)$res['type_id'] . "','" . (int)$product_id . "', '" . (int)$res['attribute_id'] . "')";
                $db->query($sql);
            }
        }
    }

}

function getGiftsCategory($product_id){
    global $db;

    $sql = "SELECT pp.product, pp.page, pc.category
        FROM gifts_product_to_page as pp
        join gifts_page_to_category as pc
        on pc.page = pp.page
        join gifts_page_percent as pper
        on pp.page = pper.page 
        WHERE pp.product = ".(int)$product_id;

    $result_db = $db->query($sql);
    $item = $result_db->fetch_array();
    if ($item) $res = $item['category'];
    else $res = false;
    return $res;
}

function getPriceWithPercent($price, $product_id){
    global $db;

    $sql = "SELECT pper.percent as percent
        FROM gifts_product_to_page as pp
        left join gifts_page_to_category as pc
        on pc.page = pp.page
        left join gifts_page_percent as pper
        on pp.page = pper.page 
        WHERE pp.product = ".(int)$product_id;

    $result_db = $db->query($sql);
    $item = $result_db->fetch_array();
    if ($item){ 
        $res = $price+round($price*$item['percent']/100); 
    }else{
        $res = $price;
    }


    return $res;
}

function addProductOption($product_id, $option_value_id, $product_option_id, $pr){
    global $db;

    if (!$option_value_id) {return false;}

    $sql = "INSERT INTO `oc_product_option_value`
(`product_option_id`, `product_id`, `option_id`,
`option_value_id`, `quantity`, `subtract`,
`price`, `price_prefix`, `points`,
`points_prefix`, `weight`, `weight_prefix`)
VALUES
(".(int)$product_option_id.", ".(int)$product_id.", 13,
".(int)$option_value_id.", ".(int)$pr['quantity'].", 1,
0, '+', 0,
'+', 0.00000000, '+')";
    $db->query($sql);
}

/**
 * Получение option_value_id
 * Сохранение размера, если нет
 * @param $size_code
 * @return bool|int|string
 */
function getOptionValueId($size_code) {
    global $size_code_arr;

    if (!in_array($size_code, $size_code_arr)) {
        // добавление размера
        global $db;
        $sql = "INSERT INTO `oc_option_value` (`option_id`, `image`, `sort_order`) VALUES (13, 'no_image.jpg', 5)";
        $db->query($sql);

        $option_id = $db->insert_id;

        $sql = "INSERT INTO `oc_option_value_description` (`option_value_id`, `language_id`, `option_id`, `name`) VALUES (" . (int)$option_id . ", 2, 13, '" . $db->sql($size_code) . "');";
        $db->query($sql);

        $size_code_arr[$option_id] = $size_code;

        return $option_id;
    } else {
        foreach ($size_code_arr as $key=>$size){
            if ($size == $size_code) {return $key;}
        }
    }

    return false;
}

/**
 * Проверка наличия продукта в БД
 * @param $sku
 * @return bool
 */
function checkProductExist($sku) {
    global $db;
    $sql = "SELECT product_id FROM " . DB_PREFIX . "product WHERE sku = '" . (int)$sku . "' LIMIT 1";
    $res = $db->query($sql);
    $product = $res->fetch_array();
    return isset($product['product_id']) ? $product['product_id'] : false;
}

function checkProductIsClothing($product_id) {
    global $db;
    $sql = "SELECT product_id FROM " . DB_PREFIX . "product_to_category WHERE product_id='".$product_id."' AND category_id IN (59,74,75,76,77,78,79) LIMIT 1";
    $res = $db->query($sql);
    $product = $res->fetch_array();
    return isset($product['product_id']) ? $product['product_id'] : false;
}

function addProduct($data) {
    global $db;
    $sql = "INSERT INTO " . DB_PREFIX . "product
        SET model = '" . $db->sql($data['model']) . "',
        sku = '" . $db->sql($data['sku']) . "',
        upc = '" . $db->sql($data['upc']) . "',
        ean = '" . $db->sql($data['ean']) . "',
        jan = '" . $db->sql($data['jan']) . "',
        isbn = '" . $db->sql($data['isbn']) . "',
        mpn = '" . $db->sql($data['mpn']) . "',
        location = '" . $db->sql($data['location']) . "',
        quantity = '" . (int)$data['quantity'] . "',
        minimum = '" . (int)$data['minimum'] . "',
        subtract = '" . (int)$data['subtract'] . "',
        stock_status_id = '" . (int)$data['stock_status_id'] . "',
        image = '" . (string)$data['image'] . "',
        date_available = '" . $db->sql($data['date_available']) . "',
        manufacturer_id = '" . (int)$data['manufacturer_id'] . "',
        shipping = '" . (int)$data['shipping'] . "',
        price = '" . (float)$data['price'] . "',
        points = '" . (int)$data['points'] . "',
        weight = '" . (float)$data['weight'] . "',
        weight_class_id = '" . (int)$data['weight_class_id'] . "',
        length = '" . (float)$data['length'] . "',
        width = '" . (float)$data['width'] . "',
        height = '" . (float)$data['height'] . "',
        length_class_id = '" . (int)$data['length_class_id'] . "',
        status = '" . (int)$data['status'] . "',
        shop = '" . (int)$data['shop'] . "',
        tax_class_id = '" . $db->sql($data['tax_class_id']) . "',
        sort_order = '" . (int)$data['sort_order'] . "',
        date_added = NOW()";
    $db->query($sql);

    $product_id = $db->insert_id;

    foreach ($data['product_description'] as $language_id => $value) {
        $sql = "INSERT INTO " . DB_PREFIX . "product_description
        SET product_id = '" . (int)$product_id . "',
        language_id = '" . (int)$language_id . "',
        name = '" . $db->sql($value['name']) . "',
        meta_keyword = '" . $db->sql($value['meta_keyword']) . "',
        meta_description = '" . $db->sql($value['meta_description']) . "',
        description = '" . $db->sql($value['description']) . "',
        tag = '" . $db->sql($value['tag']) . "'";
        $db->query($sql);
    }

    foreach ($data['product_image'] as $image) {
        $sql = "INSERT INTO " . DB_PREFIX . "product_image
        SET product_id = '" . (int)$product_id . "',
        image = '" . $image . "'";
        $db->query($sql);
    }

    // таблица привязки продукта к товару
    $sql = "INSERT into " . DB_PREFIX . "product_to_store values ('" . (int)$product_id . "','0')";
    $db->query($sql);


    if ($data['category']) {
        // определение родительских категорий
        $sql = "SELECT * from oc_category_path as cp
            where category_id in ('" . (int)$data['category'] . "')
            and `level` > 0";
        $res = $db->query($sql);

        while ($item = $res->fetch_array()) {
            // таблица привязки категории к продукту
            $sql = "INSERT into " . DB_PREFIX . "product_to_category values('" . (int)$product_id . "','" . (int)$item['path_id'] . "')";
            $db->query($sql);
        }
    }

    return $product_id;
}

function updateProduct($data, $product_id) {
    global $db;

    // Определение всех изображений товара, чтобы потом удалить лишние
    $sql = "select image from oc_product
            where product_id = '" . (int)$product_id . "'";
    $res = $db->query($sql);
    $item = $res->fetch_array();

    $product_img_del_arr = ($item['image'] == (string)$data['image']) ? array() : array($item['image'] => $product_id);

    $sql = "select * from oc_product_image
            where product_id = '" . (int)$product_id . "'";
    $res = $db->query($sql);

    while ($item = $res->fetch_array()){
        $product_img_del_arr[$item['image']] = $product_id;
    }
    //--

    $sql = "UPDATE " . DB_PREFIX . "product
        SET model = '" . $db->sql($data['model']) . "',
        upc = '" . $db->sql($data['upc']) . "',
        ean = '" . $db->sql($data['ean']) . "',
        jan = '" . $db->sql($data['jan']) . "',
        isbn = '" . $db->sql($data['isbn']) . "',
        mpn = '" . $db->sql($data['mpn']) . "',
        location = '" . $db->sql($data['location']) . "',
        quantity = '" . (int)$data['quantity'] . "',
        minimum = '" . (int)$data['minimum'] . "',
        subtract = '" . (int)$data['subtract'] . "',
        stock_status_id = '" . (int)$data['stock_status_id'] . "',
        image = '" . (string)$data['image'] . "',
        date_available = '" . $db->sql($data['date_available']) . "',
        manufacturer_id = '" . (int)$data['manufacturer_id'] . "',
        shipping = '" . (int)$data['shipping'] . "',
        price = '" . (float)$data['price'] . "',
        points = '" . (int)$data['points'] . "',
        weight = '" . (float)$data['weight'] . "',
        weight_class_id = '" . (int)$data['weight_class_id'] . "',
        length = '" . (float)$data['length'] . "',
        width = '" . (float)$data['width'] . "',
        height = '" . (float)$data['height'] . "',
        length_class_id = '" . (int)$data['length_class_id'] . "',
        status = '" . (int)$data['status'] . "',
        status_tmp = '" . (int)$data['status_tmp'] . "',
        shop = '" . (int)$data['shop'] . "',
        tax_class_id = '" . $db->sql($data['tax_class_id']) . "',
        sort_order = '" . (int)$data['sort_order'] . "',
        date_modified = NOW() 
        WHERE product_id='".$product_id."'";
    
        $db->query($sql);

    foreach ($data['product_description'] as $language_id => $value) {
        $sql = "UPDATE " . DB_PREFIX . "product_description
        SET 
        name = '" . $db->sql($value['name']) . "',
        meta_keyword = '" . $db->sql($value['meta_keyword']) . "',
        meta_description = '" . $db->sql($value['meta_description']) . "',
        description = '" . $db->sql($value['description']) . "',
        tag = '" . $db->sql($value['tag']) . "' WHERE product_id='".$product_id."' and language_id='". (int)$language_id ."'";
        $db->query($sql);
    }

    foreach ($data['product_image'] as $image) {
        $sql = "select * from oc_product_image
            where product_id = '" . (int)$product_id . "'
            AND image = '" . $image . "'
            LIMIT 1";

        $res = $db->query($sql);
        $res = $res->fetch_array();

        if(!$res) {
            $sql = "INSERT INTO " . DB_PREFIX . "product_image
                SET product_id = '" . (int)$product_id . "',
                image = '" . $image . "'";
            $db->query($sql);
        }

        if (isset($product_img_del_arr[$image])) {
            unset($product_img_del_arr[$image]);
        }
    }

    // Удаление файлов изображений, ссылок на которые нет в БД

//    foreach ($product_img_del_arr as $img => $p) {
//        echo ($product_img_del_arr[$img] . ' => ' . $img . "\n");
////        if (file_exists(DIR_IMAGE . $img)){
////            unlink(DIR_IMAGE . $img);
////        }
//    }

    // Пока не обновляем привязки к категориям

    if ($data['category']) {
        // определение родительских категорий
        $sql = "SELECT * from oc_category_path as cp
            LEFT JOIN oc_product_to_category as pc
            on pc.category_id = cp.path_id
            and pc.product_id = '" . (int)$product_id . "'
            where cp.category_id in ('" . (int)$data['category'] . "')
            and cp.`level` > 0
            and pc.product_id is NULL";
        $res = $db->query($sql);

        while ($item = $res->fetch_array()) {
            // таблица привязки категории к продукту
            $sql = "INSERT into " . DB_PREFIX . "product_to_category values('" . (int)$product_id . "','" . (int)$item['path_id'] . "')";
            $db->query($sql);

            echo "cat: " . $product_id . "," . (int)$item['path_id'] . "\n";
        }
    }

    return $product_id;


}
/////////////////////////////////////////////////

/**
 * Создание таблиц
 */
function createTables(){
    global $db;

    $sql = "CREATE TABLE IF NOT EXISTS `gifts_page` (
	`page_id` INT NOT NULL,
	`parent_page_id` INT NOT NULL DEFAULT '0',
	`name` VARCHAR(200) NOT NULL DEFAULT '',
	`uri` VARCHAR(300) NOT NULL DEFAULT '',
	PRIMARY KEY (`page_id`)
)";

    $db->query($sql);

    $sql = "CREATE TABLE IF NOT EXISTS `gifts_product_to_page` (
	`page` INT NOT NULL,
	`product` INT NOT NULL,
	PRIMARY KEY (`page`, `product`)
)";

    $db->query($sql);

    $sql = "CREATE TABLE IF NOT EXISTS `gifts_page_to_category` (
	`page` INT NOT NULL,
	`category` INT NOT NULL,
	PRIMARY KEY (`page`, `category`)
)";

    $db->query($sql);

    $sql = "CREATE TABLE IF NOT EXISTS `gifts_page_path` (
	`page_id` INT NOT NULL,
	`path_id` INT NOT NULL,
	`level` INT NOT NULL,
	`path` VARCHAR(200) NOT NULL DEFAULT '',
	PRIMARY KEY (`page_id`, `path_id`)
)";

    $db->query($sql);

    $sql = "CREATE TABLE IF NOT EXISTS `gifts_filtertype` (
	`id` INT NOT NULL,
	`name` VARCHAR(200) NOT NULL DEFAULT '',
	`attribute_id` INT NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)";

    $db->query($sql);

    $sql = "CREATE TABLE IF NOT EXISTS `gifts_filter` (
	`id` INT NOT NULL,
	`type_id` INT NOT NULL,
	`name` VARCHAR(200) NOT NULL DEFAULT '',
	PRIMARY KEY (`id`, `type_id`)
)";

    $db->query($sql);

    $sql = "CREATE TABLE IF NOT EXISTS `gifts_filter_to_product` (
	`filter_id` INT NOT NULL,
	`filtertype_id` INT NOT NULL,
	`product_id` INT NOT NULL,
	`attribute_id` INT NOT NULL,
	PRIMARY KEY (`filter_id`, `filtertype_id`, `product_id`, `attribute_id`)
)";

    $db->query($sql);
}

//////////////////////////////////////
/**
 * Заполнение таблицы путей для категории (page) - связей с родительскими категориями
 */
function fillGiftsPagePath(){
    global $db;

    // очистка таблицы `gifts_page_path`
    $sql = "TRUNCATE TABLE `gifts_page_path`";
    $db->query($sql);

    $parents = array(0);
    $level = 0;
    $parent_path = '';

    while (!empty($parents)) {
        foreach ($parents as $parent_id) {
            $sql = "SELECT path
                FROM `gifts_page_path`
                WHERE `page_id` = '" . (int)$parent_id . "'";
            $res = $db->query($sql);
            $parent_path = $res->fetch_array();
            $parent_path = $parent_path ? $parent_path['path'] : '';


            $sql = "SELECT page_id, parent_page_id
                FROM `gifts_page`
                WHERE `parent_page_id` = '" . (int)$parent_id . "'";

            $result_db = $db->query($sql);

            $parents = array();

            while ($item = $result_db->fetch_array()) {
                $parents[] = $item['page_id'];

                $sql2 = "INSERT INTO gifts_page_path SET
                    page_id = '" . (int)$item['page_id'] . "',
                    path_id = '" . (int)$parent_id . "',
                    path = '" . $db->sql($parent_path . '-' .  (int)$item['page_id']) . "',
                    level = '" . (int)$level . "'";
                $db->query($sql2);
            }
        }
        $level++;
    }
}

function getGiftsPageList() {
    global $db;
    $sql = "select p.page_id, p.name, pper.percent as percent, pp.`level`
        from gifts_page as p
        join gifts_page_path as pp
        on pp.page_id = p.page_id
        left join gifts_page_percent pper
        on p.page_id=pper.page
        order by pp.path";
    $result_db = $db->query($sql);

    $data = array();

    while ($item = $result_db->fetch_array()) {
        $data[] = $item;
    }

    return $data;
}

function getCategories() {
    global $db;

    $sql = "SELECT cp.category_id AS category_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR ' &gt; ') AS name, c.parent_id, c.sort_order FROM " . DB_PREFIX . "category_path cp
LEFT JOIN " . DB_PREFIX . "category c ON (cp.path_id = c.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (c.category_id = cd1.category_id)
LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (cp.category_id = cd2.category_id)
WHERE cd1.language_id = '2'
AND cd2.language_id = '2'
GROUP BY cp.category_id ORDER BY name";

    $result_db = $db->query($sql);

    return $db->rows($result_db);
}

function setCategoryLinks($gift_select, $gift_percent){
    global $db;

    $sql = "TRUNCATE TABLE gifts_page_to_category";
    $db->query($sql);    
    $sql = "TRUNCATE TABLE gifts_page_percent";
    $db->query($sql);

    $page_arr = array();

    foreach ($gift_select as $page=>$category ) {
        if (intval($category) > 0) {
            $sql = "INSERT INTO gifts_page_to_category SET
                    page = '" . (int)$page . "',
                    category = '" . (int)$category . "'";
            $db->query($sql);

             $sql = "INSERT INTO gifts_page_percent SET
                    page = '" . (int)$page . "',
                    percent = '" . (int)$gift_percent[$page] . "'";
            $db->query($sql);

            $page_arr[] = (int)$page;
        }
    }

    $page_sql = implode(',', $page_arr);

    if ($page_sql) {
        $sql = "REPLACE INTO oc_product_to_category
                select p.product_id, pc.category from gifts_product_to_page as pp
                join gifts_page_to_category as pc
                on pc.page = pp.page
                join oc_product as p
                on p.sku = pp.product
                where pp.page in (".$page_sql.")";

        $db->query($sql);
    }
}

function getCategoryLinks(){
    global $db;
    $sql = "SELECT * FROM gifts_page_to_category";
    $result_db = $db->query($sql);
    $data = array();
    while ($item = $result_db->fetch_array()) {
        $data[$item['page']] = $item['category'];
    }
    return $data;
}

/**
 * Сохранение изображения, если не было
 * @param $src
 * @return string - адрес изображения
 */
function saveImg($src) {
    $path = getImageAddr($src);

    if (!$path){
        return false;
    }

    $fpath = DIR_IMAGE . $path;

    if (!file_exists($fpath)) {
//        echo microtime(true) . "\n";
        sleep(0.4); // костыль для выполнения условия: не более 5 запросов в секунду.
        $file_url = 'http://14849_xmlexport:8qs6Ji44@api2.gifts.ru/export/v2/catalogue/' . $src;
        copy($file_url, $fpath);
//        echo "++\n";
    }
//    echo $path . "\n\n";
    return $path;
}

/**
 * Получение локального адреса изображения
 * @param $src
 * @return string
 */
function getImageAddr($src) {
    $upload_dir = 'data/products/';
    $filename = array_pop(explode('/', (string)$src));
//    var_dump($filename);
    return !empty($filename) ? $upload_dir . $filename : false;
}


//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

$db = new Database();


if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['gift_select'])){
    setCategoryLinks($_POST['gift_select'], $_POST['gift_percent']);
}


//////////////////////////////////////////////////////////////////////////////////

//-----------
if (!empty($_REQUEST['step0'])) {

    createTables();

    $xml = new SimpleXMLElement('gifts_files/tree.xml', NULL, TRUE);

    xmlRecurseParceTree($xml);

    fillGiftsPagePath();

    xmlParceFilters();

    echo "\n Создание/обновление таблиц выполнено.";
    exit;
}


//-----------
$giftsPageList = getGiftsPageList();

foreach ($giftsPageList as $item) {
    for ($i=0; $i<=$item['level']; $i++) {
        $item['name'] = "----" . $item['name'];
    }
}


$categories = getCategories();

$categoryLinks = getCategoryLinks();
//-----------

// для заполнения данных о товарах
if (!empty($_REQUEST['do_parse_products'])) {
    $stock = xmlParceStock();

    // массив размеров
    $size_code_arr = getSizes();

    updateTmpStatusProducts();

    xmlParceProduct($stock);

    updateActiveStatusProduct();

    echo "\n//do_parse_products!";
    exit();
}


// для получения xml
if (!empty($_REQUEST['get_stock'])) {
    exec("wget --http-user=14849_xmlexport --http-password=8qs6Ji44 http://api2.gifts.ru/export/v2/catalogue/stock.xml wait=1 --output-document=gifts_files/stock.xml");
    exit();
}




//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////



?> </pre>

<form action="get_gifts_daemon.php" method="post">
<?php
    $level_arr = array();
    foreach ($giftsPageList as $item) {
        $level_arr[$item['level']] = $item['name'];
        for ($i=0; $i<=$item['level']; $i++) {
            $item['name1'] = $level_arr[0];
            for ($j=1; $j<=$i; $j++) {
                $item['name1'] .= ' > ' . $level_arr[$j];
            }
        }
        ?>
    <dl>
        <dt><label for="gift_select_<?=$item['page_id']?>"><?= $item['name1']?></label></dt>
        <dd>
            <select id="gift_select_<?=$item['page_id']?>" name="gift_select[<?=$item['page_id']?>]">
                <option value="0"></option>
                <?foreach ($categories as $category) {?>
                <option value="<?=$category['category_id']?>"<?= isset($categoryLinks[$item['page_id']]) && $categoryLinks[$item['page_id']] == $category['category_id'] ? 'selected=selected' : '' ?>>
                    <?=$category['name']?>;
                </option>
                <?}?>
            </select>
            <input type="text" name="gift_percent[<?=$item['page_id']?>]" value="<? if($item['percent']!=NULL){ echo $item['percent'];}else{ echo 0;} ?>"/>
        </dd>
    </dl>
<?php } ?>
    <input type="submit" class="btn" value="Установить связи">
</form>