<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
<pre>
<?php

require_once "classes/database_class.php";

/**
 * @param string $url
 * @param string $params
 * @return bool|mixed
 */
function getResponse($url="", $params="") {
    //todo - настроить retry-delay

    $result = false;

    if (in_array('curl', get_loaded_extensions())) {
        $request = curl_init($url);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($request, CURLOPT_BINARYTRANSFER, 1);
        curl_setopt($request, CURLOPT_POST, true);
        curl_setopt($request, CURLOPT_POSTFIELDS, $params);
        $result = curl_exec($request);
    }

    if (!$result) {
        return false;
    }

    return $result;
}

/**
 * Рекурсивный пробег по tree.xml
 * @param $obj
 */
function xmlRecurseParceTree($obj) {
    global $db;

    $page_arr = array();
    $product_page_arr = array();

    if (isset($obj->page)) {

//        $sql = "TRUNCATE TABLE gifts_product_to_page";
//        $db->query($sql);

        foreach ($obj->page as $page) {

            $sql = "REPLACE INTO gifts_page SET
                page_id = '". (int)$page->page_id . "',
                name = '". $db->sql((string)$page->name) . "',
                uri = '". $db->sql((string)$page->uri) . "',
                parent_page_id = '". (isset($page['parent_page_id']) ? (int)$page['parent_page_id'] : 0) . "';";

            $db->query($sql);
            echo $sql . "\n";

//            $page_arr[] = array(
//                'page_id' => (int)$page->page_id,
//                'name' => (string)$page->name,
//                'uri' => (string)$page->uri,
//                'parent_page_id' => isset($page['parent_page_id']) ? (int)$page['parent_page_id'] : 0,
//            );

            if (isset($page->product)) {
                foreach ($page->product as $product) {

                    if (intval($product->page) > 0 && intval($product->product) > 0) {
                        $sql = "REPLACE INTO gifts_product_to_page SET
                    page = '" . (int)$product->page . "',
                    product = '" . (int)$product->product . "';";

                        $db->query($sql);
                        echo $sql . "\n";

//                $product_page_arr[] = array(
//                    'page'      => (int)$product->page,
//                    'product'   => (int)$product->product,
//                );
                    }
                }
            }

//            echo "\n---- page_arr\n\n";
//            print_r($page_arr);
//            echo "\n---- product_page_arr\n\n";
//            print_r($product_page_arr);

            xmlRecurseParceTree($page);
        }
    }
}


function xmlParceProduct ($obj) {
    $product_arr = array();
    $product_data = array();

    $loop = 10;

    foreach ($obj->product as $product) {
        $product_arr = array(
            'main_product' => (int)$product->product_id,
            'code' => (string)$product->code,
            'group' => (int)$product->group,
            'name' => (string)$product->name,
            'content' => (string)$product->content,
            'small_image' => (string)$product->small_image['src'],
            'big_image' => (string)$product->big_image['src'],
            'super_big_image' => (string)$product->super_big_image['src'],
            'product_size' => (string)$product->product_size,
            'price' => (string)$product->price->price,
            'currency' => (string)$product->price->currency,
            'weight' => (string)$product->weight,
        );


        $pr_arr = array();
        foreach($product->product as $pr){
            $pr_arr[] = array(
                'product_id' => (int)$pr->product_id,
                'code' => (string)$product->code,
                'size_code' => (string)$pr->size_code,
                'name' => (string)$pr->name,
                'price' => (string)$pr->price->price,
                'currency' => (string)$pr->price->currency,
            );
        }
        if ($pr_arr) {
            $product_arr['product'] = $pr_arr;
        }


        $pr_img_arr = array();
        $pr_file_arr = array();
        foreach($product->product_attachment as $pr){
            if ($pr->meaning) {
                $pr_img_arr[] = (string)$pr->image;
            } else {
                $pr_file_arr[] = (string)$pr->file;
            }
        }
        if ($pr_img_arr) {
            $product_arr['product_image'] = $pr_img_arr;
        }
        if ($pr_file_arr) {
            $product_arr['product_file'] = $pr_file_arr;
        }

        $product_data[] = $product_arr;

        insertProduct($product_arr);

        if (--$loop == 0) {
            break;
        }
    }

//    print_r($product_data);
//    exit;
}

/////////////////////////////////////////////////

function insertProduct($pr_data) {
    global $db;

    $data = array(
        'model' => $pr_data['code'],
        'sku' => $pr_data['main_product'],
        'image' => $pr_data['small_image'],
        'price' => $pr_data['price'],
        'product_description' => array(2=>array(
            'name' => $pr_data['name'],
            'description' => $pr_data['content'],
        )),
        'stock_status_id' => 1,
        'subtract' => 1,
        'status' => 1,
        'minimum' => 1,
        'sort_order' => 1,
    );

    if (!$product_id = checkExistence($pr_data['main_product'])) {
        addProduct($data);
    } else {
        updateProduct();
    }
}

function checkExistence($sku) {
    global $db;
    $sql = "SELECT product_id FROM " . DB_PREFIX . "product WHERE sku = '" . (int)$sku . "' LIMIT 1";
    $res = $db->query($sql);
    $product = $res->fetch_array();
    return isset($product['product_id']) ? $product['product_id'] : false;
}


function addProduct($data) {
    global $db;
    $sql = "INSERT INTO " . DB_PREFIX . "product
        SET model = '" . $db->sql($data['model']) . "',
        sku = '" . $db->sql($data['sku']) . "',
        upc = '" . $db->sql($data['upc']) . "',
        ean = '" . $db->sql($data['ean']) . "',
        jan = '" . $db->sql($data['jan']) . "',
        isbn = '" . $db->sql($data['isbn']) . "',
        mpn = '" . $db->sql($data['mpn']) . "',
        location = '" . $db->sql($data['location']) . "',
        quantity = '" . (int)$data['quantity'] . "',
        minimum = '" . (int)$data['minimum'] . "',
        subtract = '" . (int)$data['subtract'] . "',
        stock_status_id = '" . (int)$data['stock_status_id'] . "',
        date_available = '" . $db->sql($data['date_available']) . "',
        manufacturer_id = '" . (int)$data['manufacturer_id'] . "',
        shipping = '" . (int)$data['shipping'] . "',
        price = '" . (float)$data['price'] . "',
        points = '" . (int)$data['points'] . "',
        weight = '" . (float)$data['weight'] . "',
        weight_class_id = '" . (int)$data['weight_class_id'] . "',
        length = '" . (float)$data['length'] . "',
        width = '" . (float)$data['width'] . "',
        height = '" . (float)$data['height'] . "',
        length_class_id = '" . (int)$data['length_class_id'] . "',
        status = '" . (int)$data['status'] . "',
        tax_class_id = '" . $db->sql($data['tax_class_id']) . "',
        sort_order = '" . (int)$data['sort_order'] . "',
        date_added = NOW()";
    $db->query($sql);

    $product_id = $db->insert_id;

    foreach ($data['product_description'] as $language_id => $value) {
        $sql = "INSERT INTO " . DB_PREFIX . "product_description
        SET product_id = '" . (int)$product_id . "',
        language_id = '" . (int)$language_id . "',
        name = '" . $db->sql($value['name']) . "',
        meta_keyword = '" . $db->sql($value['meta_keyword']) . "',
        meta_description = '" . $db->sql($value['meta_description']) . "',
        description = '" . $db->sql($value['description']) . "',
        tag = '" . $db->sql($value['tag']) . "'";
        $db->query($sql);
    }
}



function updateProduct() {

}
/////////////////////////////////////////////////

/**
 * Создание таблиц таблиц
 */
function createTables(){
    global $db;

    $sql = "CREATE TABLE IF NOT EXISTS `gifts_page` (
	`page_id` INT NOT NULL,
	`parent_page_id` INT NOT NULL DEFAULT '0',
	`name` VARCHAR(200) NOT NULL DEFAULT '',
	`uri` VARCHAR(300) NOT NULL DEFAULT '',
	PRIMARY KEY (`page_id`)
)";

    $db->query($sql);

    $sql = "CREATE TABLE IF NOT EXISTS `gifts_product_to_page` (
	`page` INT NOT NULL,
	`product` INT NOT NULL,
	PRIMARY KEY (`page`, `product`)
)";

    $db->query($sql);

    $sql = "CREATE TABLE IF NOT EXISTS `gifts_page_to_category` (
	`page` INT NOT NULL,
	`category` INT NOT NULL,
	PRIMARY KEY (`page`, `category`)
)";

    $db->query($sql);

    $sql = "CREATE TABLE `gifts_page_path` (
	`page_id` INT NOT NULL,
	`path_id` INT NOT NULL,
	`level` INT NOT NULL,
	`path` VARCHAR(200) NOT NULL DEFAULT '',
	PRIMARY KEY (`page_id`, `path_id`)
)";

    $db->query($sql);
}

//////////////////////////////////////

function fillGiftsPagePath()
{
    global $db;

    // очистка таблицы `gifts_page_path`
    $sql = "TRUNCATE TABLE `gifts_page_path`";
    $db->query($sql);

    $parents = array(0);
    $level = 0;
    $parent_path = '';

    while (!empty($parents)) {
        foreach ($parents as $parent_id) {
            $sql = "SELECT path
                FROM `gifts_page_path`
                WHERE `page_id` = '" . (int)$parent_id . "'";
            $res = $db->query($sql);
            $parent_path = $res->fetch_array();
            $parent_path = $parent_path ? $parent_path['path'] : '';


            $sql = "SELECT page_id, parent_page_id
                FROM `gifts_page`
                WHERE `parent_page_id` = '" . (int)$parent_id . "'";

            $result_db = $db->query($sql);

            $parents = array();

            while ($item = $result_db->fetch_array()) {
                $parents[] = $item['page_id'];

                $sql2 = "INSERT INTO gifts_page_path SET
                    page_id = '" . (int)$item['page_id'] . "',
                    path_id = '" . (int)$parent_id . "',
                    path = '" . $db->sql($parent_path . '-' .  (int)$item['page_id']) . "',
                    level = '" . (int)$level . "'";
                $db->query($sql2);
            }
        }
        $level++;
    }
}


function getGiftsPageList() {
    global $db;
    $sql = "select p.page_id, p.name, pp.`level`
        from gifts_page as p
        join gifts_page_path as pp
        on pp.page_id = p.page_id
        order by pp.path";
    $result_db = $db->query($sql);

    $data = array();

    while ($item = $result_db->fetch_array()) {
        $data[] = $item;
    }

    return $data;
}


function getCategories() {
    global $db;

    $sql = "SELECT cp.category_id AS category_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR ' &gt; ') AS name, c.parent_id, c.sort_order
FROM " . DB_PREFIX . "category_path cp
LEFT JOIN " . DB_PREFIX . "category c ON (cp.path_id = c.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (c.category_id = cd1.category_id)
LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (cp.category_id = cd2.category_id)
WHERE cd1.language_id = '2'
AND cd2.language_id = '2'
GROUP BY cp.category_id ORDER BY name";

    $result_db = $db->query($sql);

    return $db->rows($result_db);
}



function setCategoryLinks($gift_select){
    global $db;

    $sql = "TRUNCATE TABLE gifts_page_to_category";
    $db->query($sql);

    foreach ($gift_select as $page=>$category ) {
        if (intval($category) > 0) {
            $sql = "REPLACE INTO gifts_page_to_category SET
                    page = '" . (int)$page . "',
                    category = '" . (int)$category . "'";
            $db->query($sql);
        }
    }
}

function getCategoryLinks(){
    global $db;
    $sql = "SELECT * FROM gifts_page_to_category";
    $result_db = $db->query($sql);
    $data = array();
    while ($item = $result_db->fetch_array()) {
        $data[$item['page']] = $item['category'];
    }
    return $data;
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

$db = new Database();


if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['gift_select'])){
    setCategoryLinks($_POST['gift_select']);
}




//////////////////////////////////////////////////////////////////////////////////

//-----------
createTables();

$xml = new SimpleXMLElement('gifts_files/tree.xml', NULL, TRUE);

xmlRecurseParceTree($xml);

fillGiftsPagePath();


//-----------
//$giftsPageList = getGiftsPageList();
//
//foreach ($giftsPageList as $item) {
//    for ($i=0; $i<=$item['level']; $i++) {
//        $item['name'] = "----" . $item['name'];
//    }
//}
//
//
//$categories = getCategories();
//
//$categoryLinks = getCategoryLinks();
//-----------


//$xml = new SimpleXMLElement('gifts_files/product.xml', NULL, TRUE);
//xmlParceProduct($xml);
